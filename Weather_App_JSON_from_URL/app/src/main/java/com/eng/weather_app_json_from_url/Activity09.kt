package com.eng.weather_app_json_from_url

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_09.*

class Activity09 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_09)

        val kreiraniAdapter = Adapter(DataGlobal.mainObjFilterd)
        recyclerView09.adapter = kreiraniAdapter
        recyclerView09.setLayoutManager(GridLayoutManager(this, 1))

    }
}