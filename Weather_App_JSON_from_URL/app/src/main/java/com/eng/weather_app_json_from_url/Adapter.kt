package com.eng.weather_app_json_from_url

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell.view.*

class Adapter(val mainObject: MainObject) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.tvDate.text = mainObject.list[position].dt_txt
        holder.view.tvGrad.text = mainObject.city.cityName
        holder.view.tvCountry.text = mainObject.city.country
        holder.view.tvTemp.text =
            (mainObject.list[position].main.temp / 17.77).toInt().toString() + "C"

        holder.view.tvPopulation.text = mainObject.city.population.toString() + " +/- people"
        holder.view.tvLon.text = "Longitude: " + mainObject.city.coord.lon.toString()
        holder.view.tvLat.text = "Latittude: " + mainObject.city.coord.lat.toString()
        holder.view.tvTimezone.text = "Timezone: " + mainObject.city.timezone.toString()

        holder.view.tvFeelsLike.text =
            "Feels Like: " + (mainObject.list[position].main.feels_like / 17.77).toInt()
                .toString() + "C"
        holder.view.tvTempMin.text =
            "MIN: " + (mainObject.list[position].main.temp_min / 17.77).toInt().toString() + "C"
        holder.view.tvTempMax.text =
            "MAX: " + (mainObject.list[position].main.temp_max / 17.77).toInt().toString() + "C"
        holder.view.tvPressure.text =
            "Pressure: " + mainObject.list[position].main.pressure.toString()
        holder.view.tvSeaLevel.text =
            "Sea Level:" + mainObject.list[position].main.sea_level.toString()
        holder.view.tvHumidity.text =
            "Humidity:" + mainObject.list[position].main.humidity.toString()
        holder.view.tvWeatherMain.text = mainObject.list[position].weather[0].mainWeather
        holder.view.tvDescription.text = mainObject.list[position].weather[0].description
        holder.view.tvVisible.text = mainObject.list[position].visibility.toString()
        holder.view.tvCloudsAll.text = mainObject.list[position].clouds.all.toString() + "%"
        holder.view.tvSpeed.text = mainObject.list[position].wind.speed.toString()
        holder.view.tvDeg.text = mainObject.list[position].wind.deg.toString() + "m/s"
        holder.view.tvGust.text = mainObject.list[position].wind.gust.toString()

        println(DataGlobal.mainObj.toString())


    }

    override fun getItemCount(): Int {

        return mainObject.list.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}