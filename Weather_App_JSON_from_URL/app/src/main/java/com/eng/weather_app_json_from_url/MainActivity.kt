package com.eng.weather_app_json_from_url

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.InputStream
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList


class MainObject(
    var cod: Int = 0,
    var message: Int = 0,
    var cnt: Int = 0,
    var list: ArrayList<List> = arrayListOf(),
    var city: City = City(),
) : Serializable

data class Naziv(
    var ime: String,
    var vrednost: String
)
val nazivi : ArrayList<Naziv> = arrayListOf()
var grad = ""
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etSearch.setOnClickListener {
            grad = etSearch.text.toString()
        }

        val jsonURL =
            "https://api.openweathermap.org/data/2.5/forecast?q="+grad+"&appid=356292f8304f3c240d87417fc743ddb0"

        preuzmiJSON().execute(jsonURL)

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterCities(p0.toString())
            }

        })





//        nazivi.add(Naziv("dan1","2021-06-10"))
//        nazivi.add(Naziv("dan2","2021-06-11"))
//        nazivi.add(Naziv("dan3","2021-06-12"))
//        nazivi.add(Naziv("dan4","2021-06-13"))
//        nazivi.add(Naziv("dan5","2021-06-14"))
//
//        for (i in nazivi){
//            val button = Button(this)
//
//            button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT)
//
//            button.text = i.ime
//
//            button.setOnClickListener {
//                startActivity(Intent(this, Activity09::class.java))
//                filterData(i.vrednost)
//
//            }
//
//
//
//            linearLayout.addView(button)
//        }

        //recyclerView.setLayoutManager(GridLayoutManager(this, 1))

//        btn09.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date09)
//        }
//        btn10.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date10)
//        }
//        btn11.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date11)
//        }
//        btn12.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date12)
//        }
//        btn13.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date13)
//        }
//        btn14.setOnClickListener {
//            startActivity(Intent(this, Activity09::class.java))
//            filterData(DataGlobal.date14)
//        }


    }

    private fun filterCities(toString: String) {
        val filteredCities = ArrayList<List>()
    }

    inner class preuzmiJSON : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {


            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            ispisiPodatke(result)
            println(DataGlobal.mainObj.toString())
//            val kreiraniAdapter = Adapter(DataGlobal.mainObj)
//            recyclerView.adapter = kreiraniAdapter


        }
    }

    private fun ispisiPodatke(json: String?) {


        val jsonObj = JSONObject(json)

        val cod = jsonObj.getInt("cod")
        val message = jsonObj.getInt("message")
        val cnt = jsonObj.getInt("cnt")
        val list = jsonObj.getJSONArray("list")

        list.let {
            (0 until it.length()).forEach {

                val listFromArray = list.getJSONObject(it)
                val dt = listFromArray.getString("dt")
                val main = listFromArray.getJSONObject("main")
                val temp = main.getDouble("temp")
                val feels_like = main.getDouble("feels_like")
                val temp_min = main.getDouble("temp_min")
                val temp_max = main.getDouble("temp_max")
                val pressure = main.getInt("pressure")
                val sea_level = main.getInt("sea_level")
                val grnd_level = main.getInt("grnd_level")
                val humidity = main.getInt("humidity")
                val temp_kf = main.getInt("temp_kf")


                val mainObj: Main = Main(temp,
                    feels_like,
                    temp_min,
                    temp_max,
                    pressure,
                    sea_level,
                    grnd_level,
                    humidity,
                    temp_kf)

                val weather = listFromArray.getJSONArray("weather")
                weather.let {
                    (0 until it.length()).forEach {
                        val weatherFromArray = weather.getJSONObject(it)

                        val id = weatherFromArray.getInt("id")
                        val mainWeather = weatherFromArray.getString("main")
                        val description = weatherFromArray.getString("description")
                        val icon = weatherFromArray.getString("icon")

                        val weatherObj: Weather = Weather(id, mainWeather, description, icon)

                        DataGlobal.weatherList.add(weatherObj)

                    }
                }

                val clouds = listFromArray.getJSONObject("clouds")
                val all = clouds.getInt("all")

                val cloudsObj: Clouds = Clouds(all)

                val wind = listFromArray.getJSONObject("wind")
                val speed = wind.getDouble("speed")
                val deg = wind.getInt("deg")
                val gust = wind.getDouble("gust")

                val windObj: Wind = Wind(speed, deg, gust)

                val visibility = listFromArray.getInt("visibility")
                val pop = listFromArray.getInt("pop")

                val sys = listFromArray.getJSONObject("sys")
                val pod = sys.getString("pod")

                val sysObj: Sys = Sys(pod)

                val dt_txt = listFromArray.getString("dt_txt")


                val listObj: List = List(dt,
                    mainObj,
                    DataGlobal.weatherList,
                    cloudsObj,
                    windObj,
                    visibility,
                    pop,
                    sysObj,
                    dt_txt)
                DataGlobal.listList.add(listObj)
            }

        }

        val city = jsonObj.getJSONObject("city")
        val idCity = city.getInt("id")
        val name = city.getString("name")
        val coord = city.getJSONObject("coord")

        val lat = coord.getDouble("lat")
        val lon = coord.getDouble("lon")

        val coordObj: Coord = Coord(lat, lon)

        val country = city.getString("country")
        val population = city.getInt("population")
        val timezone = city.getInt("timezone")
        val sunrise = city.getInt("sunrise")
        val sunset = city.getInt("sunset")

        val cityObj: City =
            City(idCity, name, coordObj, country, population, timezone, sunrise, sunset)
        DataGlobal.mainObj = MainObject(cod, message, cnt, DataGlobal.listList, cityObj)

    }

    fun filterData(filter: String) {
        DataGlobal.mainObjFilterd = MainObject()
        for (i in DataGlobal.mainObj.list) {
            if (i.dt_txt.contains(filter)) {
                DataGlobal.mainObjFilterd.city = DataGlobal.mainObj.city
                DataGlobal.mainObjFilterd.cnt = DataGlobal.mainObj.cnt
                DataGlobal.mainObjFilterd.cod = DataGlobal.mainObj.cod
                DataGlobal.mainObjFilterd.message = DataGlobal.mainObj.message
                DataGlobal.mainObjFilterd.list.add(i)
            }

        }
    }

}

