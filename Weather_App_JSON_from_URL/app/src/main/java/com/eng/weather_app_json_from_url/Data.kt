package com.eng.weather_app_json_from_url

import java.io.Serializable

object  DataGlobal{

    var mainObj = MainObject()
    var listList: ArrayList<List> = arrayListOf()
    var weatherList: ArrayList<Weather> = arrayListOf()

    val date10 = "2021-06-10"
    val date11 = "2021-06-11"
    val date12 = "2021-06-12"
    val date13 = "2021-06-13"
    val date14 = "2021-06-14"
    val date15 = "2021-06-15"

    var mainObjFilterd = MainObject()

}

class Coord(
    var lat: Double = 0.0,
    var lon: Double = 0.0,
) : Serializable

class City(
    var idCity: Int = 0,
    var cityName: String = "",
    var coord: Coord = Coord(),
    var country: String = "",
    var population: Int = 0,
    var timezone: Int = 0,
    var sunrise: Int = 0,
    var sunset: Int = 0,
) : Serializable

class Main(
    var temp: Double = 0.0,
    var feels_like: Double = 0.0,
    var temp_min: Double = 0.0,
    var temp_max: Double = 0.0,
    var pressure: Int = 0,
    var sea_level: Int = 0,
    var grnd_level: Int = 0,
    var humidity: Int = 0,
    var temp_kf: Int = 0,
) : Serializable

class Weather(
    var idWeather: Int = 0,
    var mainWeather: String = "",
    var description: String = "",
    var icon: String = "",
) : Serializable

class Clouds(
    var all: Int = 0,
) : Serializable

class Wind(
    var speed: Double = 0.0,
    var deg: Int = 0,
    var gust: Double = 0.0,
) : Serializable

class Sys(
    pod: String = "",
) : Serializable

class List(
    var dt: String = "",
    var main: Main = Main(),
    var weather: ArrayList<Weather> = arrayListOf<Weather>(),
    var clouds: Clouds = Clouds(),
    var wind: Wind = Wind(),
    var visibility: Int = 0,
    var pop: Int = 0,
    var sys: Sys = Sys(),
    var dt_txt: String = "",
) : Serializable

