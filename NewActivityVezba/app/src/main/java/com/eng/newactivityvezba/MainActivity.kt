package com.eng.newactivityvezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnOne = findViewById<Button>(R.id.btnActivityOne)
        btnOne.setOnClickListener {
            startActivity(Intent(this, ActivityOne::class.java))
            finish()
        }
        val btnTwo = findViewById<Button>(R.id.btnActivityTwo)
        btnTwo.setOnClickListener {
            startActivity(Intent(this, ActivityTwo::class.java))
            finish()
        }
        val btnThree = findViewById<Button>(R.id.btnActivityThree)
        btnThree.setOnClickListener {
            startActivity(Intent(this, ActivityThree::class.java))
            finish()
        }

    }


}