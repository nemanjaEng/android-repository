package com.eng.newactivityvezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ActivityThree : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_three)


        val btnMain = findViewById<Button>(R.id.btnActivityMain)
        btnMain.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        val btnOne = findViewById<Button>(R.id.btnActivityOne)
        btnOne.setOnClickListener {
            startActivity(Intent(this, ActivityOne::class.java))

        }
        val btnTwo = findViewById<Button>(R.id.btnActivityTwo)
        btnTwo.setOnClickListener {
            startActivity(Intent(this, ActivityTwo::class.java))

        }
    }
}