package com.eng.newactivityvezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ActivityOne : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        val btnMain = findViewById<Button>(R.id.btnActivityMain)
        btnMain.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        val btnTwo = findViewById<Button>(R.id.btnActivityTwo)
        btnTwo.setOnClickListener {
            startActivity(Intent(this, ActivityTwo::class.java))

        }
        val btnThree = findViewById<Button>(R.id.btnActivityThree)
        btnThree.setOnClickListener {
            startActivity(Intent(this, ActivityThree::class.java))

        }
    }
}