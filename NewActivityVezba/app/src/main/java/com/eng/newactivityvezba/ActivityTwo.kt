package com.eng.newactivityvezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ActivityTwo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two)

        val btnMain = findViewById<Button>(R.id.btnActivityMain)
        btnMain.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        val btnOne = findViewById<Button>(R.id.btnActivityOne)
        btnOne.setOnClickListener {
            startActivity(Intent(this, ActivityOne::class.java))

        }
        val btnThree = findViewById<Button>(R.id.btnActivityThree)
        btnThree.setOnClickListener {
            startActivity(Intent(this, ActivityThree::class.java))

        }
    }
}