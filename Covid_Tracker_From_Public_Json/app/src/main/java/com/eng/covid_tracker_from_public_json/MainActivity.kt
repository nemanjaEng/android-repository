package com.eng.covid_tracker_from_public_json

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

val jsonHelper: JsonHelper = JsonHelper()

class ChangeSummary(
    var totalCases: Int = 0,
    var activeCases: Int = 0,
    var deaths: Int = 0,
    var recovered: Int = 0,
    var critical: Int? = null,
    var tested: Long? = null,
    var deathRatio: Double = 0.0,
    var recoveryRatio: Double = 0.0,
) : Serializable

class Country(
    var name: String = "",
    var iso3166a2: String = "",
    var iso3166a3: String = "",
    var iso3166numeric: String = "",
    var total_cases: Int = 0,
    var active_cases: Int = 0,
    var deaths: Int = 0,
    var recovered: Int = 0,
    var critical: Int = 0,
    var tested: Int = 0,
    var death_ratio: Double = 0.0,
    var recovery_ratio: Double = 0.0,
    var change: ChangeSummary = ChangeSummary(),

    ) : Serializable

class AllData(
    var summary: ChangeSummary = ChangeSummary(),
    var change: ChangeSummary = ChangeSummary(),
    var generated_on: Int = 0,
    var regions: ArrayList<Country> = arrayListOf(),
) : Serializable
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://api.quarantine.country/api/v1/summary/latest"
        takeJSON().execute(jsonURL)

        btnCountris.setOnClickListener {
            startActivity(Intent(this, CountriesActivity::class.java))
        }
    }

    inner class takeJSON : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {


            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            writeData(result)

            tvTotalCases.text = "Total cases: " + DataGlobal.allData.summary.totalCases.toString()
            tvActiveCases.text =
                "Active cases: " + DataGlobal.allData.summary.activeCases.toString()
            tvDeaths.text = "Deaths: " + DataGlobal.allData.summary.deaths.toString()
            tvRecovered.text = "Recovered: " + DataGlobal.allData.summary.recovered.toString()
            tvCritical.text = "Critical: " + DataGlobal.allData.summary.critical.toString()
            tvTested.text = "Tested: " + DataGlobal.allData.summary.tested.toString()
            tvDeathRation.text = "Death Ration: " + DataGlobal.allData.summary.deathRatio.toString()
                .format(Locale.ENGLISH, this).toFloat().toString()
            tvRecoveryRatio.text =
                "Recovery Ratio: " + DataGlobal.allData.summary.recoveryRatio.toString()
                    .format(Locale.ENGLISH, this).toFloat().toString()

            tvTotalCasesDaily.text =
                "Total cases: " + DataGlobal.allData.change.totalCases.toString()
            tvActiveCasesDaily.text =
                "Active cases: " + DataGlobal.allData.change.activeCases.toString()
            tvDeathsDaily.text = "Deaths: " + DataGlobal.allData.change.deaths.toString()
            tvRecoveredDaily.text = "Recovered: " + DataGlobal.allData.change.recovered.toString()
            tvCriticalDaily.text = "Critical: " + DataGlobal.allData.change.critical.toString()
            tvTestedDaily.text = "Tested: " + DataGlobal.allData.change.tested.toString()
            tvDeathRationDaily.text =
                "Death Ration: " + DataGlobal.allData.change.deathRatio.toString()
                    .format(Locale.ENGLISH, this).toFloat().toString()
            tvRecoveryRatioDaily.text =
                "Recovery Ratio: " + DataGlobal.allData.change.recoveryRatio.toString()
                    .format(Locale.ENGLISH, this).toFloat().toString()

        }
    }

    private fun writeData(json: String?) {

        val jsonObj = JSONObject(json)


        val data = jsonObj.getJSONObject("data")
        val summary = data.getJSONObject("summary")
        val total_casesSummary = summary.getInt("total_cases")
        val active_casesSummary = summary.getInt("active_cases")
        val deathsSummary = summary.getInt("deaths")
        val recoveredSummary = summary.getInt("recovered")
        val criticalSummary = summary.getInt("critical")
        val testedSummary = summary.getLong("tested")
        val death_ratioSummary = summary.getDouble("death_ratio")
        val recovery_ratioSummary = summary.getDouble("recovery_ratio")

        val summaryObj = ChangeSummary(total_casesSummary,
            active_casesSummary,
            deathsSummary,
            recoveredSummary,
            criticalSummary,
            testedSummary,
            death_ratioSummary,
            recovery_ratioSummary)

        val change = data.getJSONObject("change")
        val total_casesChange = change.getInt("total_cases")
        val active_casesChange = change.getInt("active_cases")
        val deathsChange = change.getInt("deaths")
        val recoveredChange = change.getInt("recovered")
        val criticalChange = change.getInt("critical")
        val testedChange = change.getLong("tested")
        val death_ratioChange = change.getDouble("death_ratio")
        val recovery_ratioChange = change.getDouble("recovery_ratio")

        val changeObj = ChangeSummary(total_casesChange,
            active_casesChange,
            deathsChange,
            recoveredChange,
            criticalChange,
            testedChange,
            death_ratioChange,
            recovery_ratioChange)

        val generated_on = data.getInt("generated_on")

        val regions = data.getJSONObject("regions")
//        val regionMap: Map<String, Country> = jsonHelper.toMap(regions) as Map<String, Country>
//        val valueListOfRegions: ArrayList<Country> = ArrayList(regionMap.values)
        for (name in 0 until regions.names().length()) {
            DataGlobal.nizDrzava.add(procitajPodatkeZaDrzavu(regions.getJSONObject(regions.names()[name].toString())))
        }

        DataGlobal.allData = AllData(summaryObj, changeObj, generated_on, DataGlobal.nizDrzava)
    }


    fun procitajPodatkeZaDrzavu(drzava: JSONObject): Country {
        val name = drzava.getString("name")
        val iso3166a2 = drzava.getString("iso3166a2")
        val iso3166a3 = drzava.getString("iso3166a3")
        val iso3166numeric = drzava.getString("iso3166numeric")
        val total_cases = drzava.getInt("total_cases")
        val active_cases = drzava.getInt("active_cases")
        val deaths = drzava.getInt("deaths")
        val recovered = drzava.getInt("recovered")
        val critical = drzava.getInt("critical")
        val tested = drzava.getInt("tested")
        val death_ratio = drzava.getDouble("death_ratio")
        val recovery_ratio = drzava.getDouble("recovery_ratio")
        val change = drzava.getJSONObject("change")
        val total_cases_change = change.getInt("total_cases")
        val active_cases_change = change.getInt("active_cases")
        val deaths_change = change.getInt("deaths")
        val recovered_change = change.getInt("recovered")
        val death_ratio_change = change.getDouble("death_ratio")
        val recovery_ratio_change = change.getDouble("recovery_ratio")

        var changeSummary: ChangeSummary = ChangeSummary(total_cases_change,
            active_cases_change,
            deaths_change,
            recovered_change,
            null,
            null,
            death_ratio_change,
            recovery_ratio_change)

            val c: Country = Country(name,
            iso3166a2,
            iso3166a3,
            iso3166numeric,
            total_cases,
            active_cases,
            deaths,
            recovered,
            critical,
            tested,
            death_ratio,
            recovery_ratio,
            changeSummary)
      return  c

    }


}

