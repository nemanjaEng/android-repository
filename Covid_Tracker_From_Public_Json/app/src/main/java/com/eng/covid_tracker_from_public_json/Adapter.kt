package com.eng.weather_app_json_from_url

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eng.covid_tracker_from_public_json.AllData
import com.eng.covid_tracker_from_public_json.Country
import com.eng.covid_tracker_from_public_json.R
import kotlinx.android.synthetic.main.cell.view.*
import java.util.*
import kotlin.collections.ArrayList

class Adapter(val nizDrzava: ArrayList<Country> ) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.tvCountryNameCell.text = nizDrzava[position].name
        holder.view.tviso3166a2Cell.text = nizDrzava[position].iso3166a2
        holder.view.tviso3166a3Cell.text = nizDrzava[position].iso3166a3
        holder.view.tvTotalCasesCell.text ="Total Cases: " + nizDrzava[position].total_cases.toString()
        holder.view.tvActiveCasesCell.text ="Active Cases: " + nizDrzava[position].active_cases.toString()
        holder.view.tvDeathsCell.text ="Deaths: " + nizDrzava[position].deaths.toString()
        holder.view.tvRecoveredCell.text ="Recovered: " + nizDrzava[position].recovered.toString()
        holder.view.tvCriticalCell.text ="Critical: " + nizDrzava[position].deaths.toString()
        holder.view.tvTestedCell.text ="Tested: " + nizDrzava[position].tested.toString()
        holder.view.tvDeathRationCell.text ="Death Ratio: " + nizDrzava[position].death_ratio.toString().format(
            Locale.ENGLISH, this).toFloat().toString()
        holder.view.tvRecoveryRatioCell.text ="Recovery Ratio: " + nizDrzava[position].recovery_ratio.toString().format(Locale.ENGLISH, this).toFloat().toString()
        holder.view.tvTotalCasesDailyCell.text ="Total Cases: " + nizDrzava[position].change.totalCases
        holder.view.tvActiveCasesDailyCell.text ="Active Cases: " + nizDrzava[position].change.activeCases
        holder.view.tvDeathsDailyCell.text ="Deaths: " + nizDrzava[position].change.deaths
        holder.view.tvRecoveredDailyCell.text ="Recovered: " + nizDrzava[position].change.recovered
        holder.view.tvDeathRationDailyCell.text ="Death Ratio: " + nizDrzava[position].change.deathRatio.toString().format(
            Locale.ENGLISH, this).toFloat().toString()
        holder.view.tvRecoveryRatioDailyCell.text ="Recovery Ratio: " + nizDrzava[position].change.recoveryRatio.toString().format(
            Locale.ENGLISH, this).toFloat().toString()

    }

    override fun getItemCount(): Int {

        return nizDrzava.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}