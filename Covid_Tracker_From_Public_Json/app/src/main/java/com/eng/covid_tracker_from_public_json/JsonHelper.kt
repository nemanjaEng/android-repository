package com.eng.covid_tracker_from_public_json

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class JsonHelper {


    @Throws(JSONException::class)
    fun getMap(obj: JSONObject, key: String?): Map<String?, Any?> {
        return toMap(obj.getJSONObject(key))
    }

    @Throws(JSONException::class)
    fun toMap(obj: JSONObject): Map<String?, Any?> {
        val map: MutableMap<String?, Any?> = mutableMapOf()
        val keys: Iterator<*> = obj.keys()
        while (keys.hasNext()) {
            val key = keys.next() as String
            map[key] = fromJson(obj[key])
        }
        return map
    }


    @Throws(JSONException::class)
    private fun fromJson(json: Any): Any? {
        return if (json === JSONObject.NULL) {
            null
        } else if (json is JSONObject) {
            toMap(json)
        } else {
            json
        }
    }
}