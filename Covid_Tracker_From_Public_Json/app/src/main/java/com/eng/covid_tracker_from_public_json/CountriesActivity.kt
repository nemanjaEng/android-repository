package com.eng.covid_tracker_from_public_json

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.GridLayoutManager
import com.eng.weather_app_json_from_url.Adapter
import kotlinx.android.synthetic.main.activity_countries.*

class CountriesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)

        val kreiraniAdapter = Adapter(DataGlobal.nizDrzava)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterCountries(p0.toString())
            }

        })
    }

    fun filterCountries(input: String){
        val filteredCountries= ArrayList<Country>()

        for( c in DataGlobal.allData.regions){
            var match = c.name.lowercase()
            if (match.contains(input.lowercase())){
                filteredCountries.add(c)
            }
        }


        val kreiraniAdapter = Adapter(filteredCountries)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))

    }
}