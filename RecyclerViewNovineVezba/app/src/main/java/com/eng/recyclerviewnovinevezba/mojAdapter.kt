package com.eng.recyclerviewnovinevezba

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

class mojAdapter(val preuzetiPodaci:ArrayList<Novine>) : RecyclerView.Adapter<CustomViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent,false)
        return CustomViewHolder(cellForRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.naslov.text = preuzetiPodaci[position].naslov
        holder.view.tekst.text = preuzetiPodaci[position].tekst
        holder.view.imageView.setImageResource(preuzetiPodaci[position].slika)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

}

class CustomViewHolder(val view:View) : RecyclerView.ViewHolder(view) {


}
