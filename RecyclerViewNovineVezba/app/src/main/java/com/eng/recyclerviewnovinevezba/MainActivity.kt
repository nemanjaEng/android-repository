package com.eng.recyclerviewnovinevezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

data class Novine (val naslov: String, val tekst: String, val slika: Int )

class MainActivity : AppCompatActivity() {

    val objectArray: ArrayList<Novine> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            createDataSource()

        val kreiraniAdapter = mojAdapter(objectArray)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }

    private fun createDataSource() {
        objectArray.add(Novine(getString(R.string.naslov1), getString(R.string.tekst1), R.drawable.slika1))
        objectArray.add(Novine(getString(R.string.naslov2), getString(R.string.tekst2), R.drawable.slika2))
        objectArray.add(Novine(getString(R.string.naslov3), getString(R.string.tekst3), R.drawable.slika3))
        objectArray.add(Novine(getString(R.string.naslov4), getString(R.string.tekst4), R.drawable.slika4))
        objectArray.add(Novine(getString(R.string.naslov5), getString(R.string.tekst5), R.drawable.slika5))
    }


}