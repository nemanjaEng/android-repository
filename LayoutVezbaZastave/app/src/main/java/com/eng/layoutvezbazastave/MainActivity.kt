package com.eng.layoutvezbazastave

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var otvoren: Boolean = false

        tvPrikazi.setOnClickListener {
            if (!otvoren) {
                loHiden.visibility = View.VISIBLE


            } else {
                loHiden.visibility = View.GONE


            }
            otvoren = !otvoren
        }
    }
}