package com.eng.dinamickiobjektivezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val layout = findViewById<LinearLayout>(R.id.linear)

        val button = Button(this)

        button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        button.text = "Klikni"
        button.setOnClickListener {

            kreirajTextView()

        }
        layout.addView(button)

    }
    private fun kreirajTextView(){
        val layout = findViewById<LinearLayout>(R.id.linear)

        val tv = TextView(this)
        layout.addView(tv)
        for( i in 0..9){

            tv.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            tv.text = "Text" + i.toString()


        }



    }
}