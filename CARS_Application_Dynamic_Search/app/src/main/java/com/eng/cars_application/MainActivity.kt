package com.eng.cars_application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable


var logovanje = false

data class Car(
    val elements: List<String>,
    var image: Int
) : Serializable

data class Logo(
    val name: String,
    val imageLogo: Int
) : Serializable


data class ModelImage(
    val name: String,
    val imageModel: Int
) : Serializable

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        if (logovanje && DataGlobal.usernameLogin == DataGlobal.user && DataGlobal.passwordLogin == DataGlobal.pass) {
//            Log.d("LOGIN", "Korisnik je ulogovan")
//        } else {
//            Log.d("LOGIN", "Korisnik nije ulogovan")
//            startActivity(Intent(this, LoginActivity::class.java))
//            finish()
//        }

        parseCSV()
        createLogoDataSource()
        parseFirstRowCSV()
        createModelImageDataSource()


        btnSeeAllProducts.setOnClickListener {
            startActivity(Intent(this, LogosListActivity::class.java))
        }

        //println(DataGlobal.headings.toString())

    }


    fun parseCSV() {


        var line: String?
        val openCSV = InputStreamReader(assets.open("modelExcelNew.csv"))
        val readLineInCSV = BufferedReader(openCSV)

        readLineInCSV.readLine()

        while (readLineInCSV.readLine().also {
                line = it
            } != null) {

            val row: List<String> = line!!.split(";")
            if (row[0].isNotEmpty()) {

                DataGlobal.cars.add(
                    Car(
                        row,
                        0

                    )
                )
            }

        }
        Log.d("CSV", DataGlobal.cars.count().toString())

    }

    fun parseFirstRowCSV() {


        val openCSV = InputStreamReader(assets.open("modelExcelNew.csv"))
        val readLineInCSV = BufferedReader(openCSV)

        val row: List<String> = readLineInCSV.readLine()!!.split(";")
        DataGlobal.headings.add(Car(row, 0))


    }

    fun createLogoDataSource() {
        DataGlobal.logos.add(Logo("Audi", R.drawable.audi))
        DataGlobal.logos.add(Logo("Mercedes-Benz", R.drawable.mercedes_benz))
        DataGlobal.logos.add(Logo("Alfa Romeo", R.drawable.alfa_romeo))
        DataGlobal.logos.add(Logo("Citroen", R.drawable.citroen))
        DataGlobal.logos.add(Logo("Dacia", R.drawable.dacia))
        DataGlobal.logos.add(Logo("Fiat", R.drawable.fiat))
        DataGlobal.logos.add(Logo("Honda", R.drawable.honda))
        DataGlobal.logos.add(Logo("Hyundai", R.drawable.hyundai))
        DataGlobal.logos.add(Logo("Infiniti", R.drawable.infiniti))
        DataGlobal.logos.add(Logo("Isuzu", R.drawable.isuzu))
        DataGlobal.logos.add(Logo("Jeep", R.drawable.jeep))
        DataGlobal.logos.add(Logo("Lada", R.drawable.lada))
        DataGlobal.logos.add(Logo("Mini", R.drawable.mini))
        DataGlobal.logos.add(Logo("Ford", R.drawable.ford))
        DataGlobal.logos.add(Logo("Mitsubishi", R.drawable.mitsubishi))
        DataGlobal.logos.add(Logo("Subaru", R.drawable.subaru))
        DataGlobal.logos.add(Logo("Nissan", R.drawable.nissan))
        DataGlobal.logos.add(Logo("Opel", R.drawable.opel))
        DataGlobal.logos.add(Logo("Peugeot", R.drawable.peugeot))
        DataGlobal.logos.add(Logo("Volkswagen", R.drawable.volkswagen))
        DataGlobal.logos.add(Logo("Volvo", R.drawable.volvo))
        DataGlobal.logos.add(Logo("Renault", R.drawable.renault))
        DataGlobal.logos.add(Logo("Seat", R.drawable.seat))
        DataGlobal.logos.add(Logo("Suzuki", R.drawable.suzuki))
        DataGlobal.logos.add(Logo("Skoda", R.drawable.skoda))
        DataGlobal.logos.add(Logo("Mazda", R.drawable.mazda))

    }

    fun createModelImageDataSource() {
        DataGlobal.imagesOfModels.add(
            ModelImage(
                "A1 sportback S line(2018)30 TFSI (116 hp)",
                R.drawable.a1_sportback_2018

            )
        )
        DataGlobal.imagesOfModels.add(
            ModelImage(
                "A1 sportback advanced (2018)30 TFSI (116 hp) S tronic",
                R.drawable.a1_sportback_s_line_2018

            )
        )
        DataGlobal.imagesOfModels.add(
            ModelImage(
                "A-klasaA 180d",
                R.drawable.a_180d
            )
        )

        DataGlobal.imagesOfModels.add(
            ModelImage(
                "A-klasaA 220 4M",
                R.drawable.a200
            )
        )
    }

}