package com.eng.cars_application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val markDetails = intent.getStringExtra("marka")
        val modelDetails = intent.getStringExtra("model")
        val motorDetails = intent.getStringExtra("motor")

        tvMarkDetails.text = markDetails
        tvModelDetails.text = modelDetails
        tvMotorDetails.text = motorDetails


        var listCarDetails: ArrayList<Car> = arrayListOf()
        for (c in DataGlobal.cars) {
            if (c.elements[0] == markDetails && c.elements[1] == modelDetails && c.elements[2] == motorDetails) {
                listCarDetails.add(c)
            }
        }


        val headingsFourToTen = DataGlobal.headings[0].elements.slice(3..9)
        val detailsFourToTen = listCarDetails[0].elements.slice(3..9)
//        println(headingsFourToTen.toString())
//        println(detailsFourToTen.toString())

        for (h in headingsFourToTen) {
            tv4_10HeadingDetails.append(h)
            tv4_10HeadingDetails.append("\n")
            tv4_10HeadingDetails.append("\n")

        }

        for (l in detailsFourToTen) {

            tv4_10Details.append(l)
            tv4_10Details.append("\n")
            tv4_10Details.append("\n")

        }

        val headingsInvisible = DataGlobal.headings[0].elements.slice(10..DataGlobal.headings[0].elements.count() - 1)
        var listInvisible = listCarDetails[0].elements.slice(10..listCarDetails[0].elements.count() - 1)
//        println(headingsInvisible.toString())
//        println(listInvisible.toString())
        for (h in headingsInvisible) {
            tvNotVisibleHeadingDetails.append(h)
            tvNotVisibleHeadingDetails.append("\n")
            tvNotVisibleHeadingDetails.append("\n")

        }
        for (l in listInvisible) {

            tvNotVisibleDetails.append(l)
            tvNotVisibleDetails.append("\n")
            tvNotVisibleDetails.append("\n")

        }

        var otvoren = false

        tvPrikazi.setOnClickListener {
            if (!otvoren) {
                loHiden.visibility = View.VISIBLE


            } else {
                loHiden.visibility = View.GONE


            }
            otvoren = !otvoren
        }


    }
}