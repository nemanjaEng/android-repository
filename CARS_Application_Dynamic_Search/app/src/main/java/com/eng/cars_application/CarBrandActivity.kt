package com.eng.cars_application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_car_brand.*
import kotlinx.android.synthetic.main.activity_logos_list.*
import kotlinx.android.synthetic.main.car_brand_cell.*

class CarBrandActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_brand)
        ivCardBrand.setImageResource(DataGlobal.logo)

        var newListofCarModel: ArrayList<Car> = arrayListOf()
        for (c in DataGlobal.cars) {
            if (c.elements[0] == DataGlobal.carModel) {
                newListofCarModel.add(c)
            }
        }
        Log.d("LISTA", newListofCarModel.toString())

        for (car in newListofCarModel) {

            for (img in DataGlobal.imagesOfModels) {
                var modelMotor = car.elements[1] + car.elements[2]
                if (modelMotor == img.name)
                    car.image = img.imageModel

            }
        }
        Log.d("LIST", newListofCarModel.toString())


            val carBrandAdapter = CarBrandAdapter(newListofCarModel, DataGlobal.logo)
            recyclerViewCarBrand.adapter = carBrandAdapter
            recyclerViewCarBrand.setLayoutManager(GridLayoutManager(this, 2))



    }

    override fun onBackPressed() {
        DataGlobal.searchedCars = arrayListOf()
        DataGlobal.clickCounter = 0
        super.onBackPressed()


    }
}