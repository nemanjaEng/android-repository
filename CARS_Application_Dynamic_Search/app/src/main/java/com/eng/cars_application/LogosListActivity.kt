package com.eng.cars_application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_car_brand.*
import kotlinx.android.synthetic.main.activity_logos_list.*

class LogosListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logos_list)

filterCars("audi a3")


            val kreiraniAdapter = MyAdapter(DataGlobal.logos)
            recyclerViewLogo.adapter = kreiraniAdapter
            recyclerViewLogo.setLayoutManager(GridLayoutManager(this, 2))


        etTextForSearch.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterCars(p0.toString())
            }

        })

    }

    fun filterCars(input: String) {
       // var textForSearch = etTextForSearch.text.toString().lowercase()
        val filtredCars = ArrayList<Car>()
        for (car in DataGlobal.cars) {
            var match = car.elements[0].lowercase() + " " + car.elements[1].lowercase()
            if (match.contains(input.lowercase())) {

                filtredCars.add(car)

            }
        }
        println(DataGlobal.searchedCars.count().toString())

        if(input == ""){
            val kreiraniAdapter = MyAdapter(DataGlobal.logos)
            recyclerViewLogo.adapter = kreiraniAdapter
            recyclerViewLogo.setLayoutManager(GridLayoutManager(this, 2))
        }else{
            val BrandAdapter = CarBrandAdapter(filtredCars, null)
            BrandAdapter.notifyDataSetChanged()
            recyclerViewLogo.adapter = BrandAdapter
            recyclerViewLogo.setLayoutManager(GridLayoutManager(this, 2))
        }

    }


}