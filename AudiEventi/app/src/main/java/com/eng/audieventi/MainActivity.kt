package com.eng.audieventi

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getSupportActionBar()?.hide()



        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        ivMenu.setOnClickListener {

            val menu = nav_view.menu
            val programma = menu.findItem(R.id.programma)
            val foodExpirience = menu.findItem(R.id.foodExpirience)
            val luoghieETerritorio = menu.findItem(R.id.luoghiETerritorio)
            val audiDrivingExperience = menu.findItem(R.id.adeMenuItem)
            val sondaggio = menu.findItem(R.id.sondaggio)
            val infoContatti = menu.findItem(R.id.infoContatti)

            if (!DataGlobal.isVisibleMenuItem) {

                programma.setVisible(false)
                foodExpirience.setVisible(false)
                luoghieETerritorio.setVisible(false)
                audiDrivingExperience.setVisible(false)
                sondaggio.setVisible(false)
                infoContatti.setVisible(false)

            } else {
                programma.setVisible(true)
                foodExpirience.setVisible(true)
                luoghieETerritorio.setVisible(true)
                audiDrivingExperience.setVisible(true)
                sondaggio.setVisible(true)
                infoContatti.setVisible(true)

            }
            drawerLayout.openDrawer(nav_view)

        }

        nav_view.setNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.home ->  closeMenuAndShowHomeFragment()
                R.id.programma -> null
                R.id.foodExpirience -> null
                R.id.luoghiETerritorio -> null
                R.id.adeMenuItem -> null
                R.id.sondaggio -> null
                R.id.calendarioEventi -> closeMenuAndShowCalendarioEventiFragment()
                R.id.couponEventi -> closeDrawlerAndOpenCouponDialog()
                R.id.infoContatti -> null

            }

            true
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    fun closeDrawlerAndOpenCouponDialog() {
        drawerLayout.closeDrawer(nav_view)
        CouponDialogAlert().showDialog(this)
    }

    fun closeMenuAndShowCalendarioEventiFragment(){
        drawerLayout.closeDrawer(nav_view)
        swapFragment(CalendarioEventiFragment())
    }
    fun closeMenuAndShowHomeFragment(){
        drawerLayout.closeDrawer(nav_view)
        swapFragment(HomeFragment())
    }

    fun swapFragment(fragment: Fragment) = supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragmentContainerView, fragment)
        commit()
    }






}