package com.eng.audieventi

import android.graphics.Bitmap
import android.widget.ArrayAdapter
import java.net.URL
import java.sql.Date

object DataGlobal {


    val dataFunctions: DataFunctions = DataFunctions()

    val clientId = "abc123"
    val clientSecret = "abcd123"
    val urlAudiEventi = "http://mobileapp-coll.engds.it/AudiEventi/"

    var numberOfLangs = 0
    var isVisibleMenuItem = false
    var audiEventiLangs: Map<String, String> = mapOf()


    //The context path for accessing public content for GUEST users is: /audi/api/public/
    val listOfPublicEvents: ArrayList<PublicEvent> = arrayListOf()
    var coupon : Coupon =  Coupon("","", "","","","", "", "","")
    //The context path for accessing premium content for COUPON users is: /audi/api/premium/
    val listOfPremiumEvents: ArrayList<PremiumEvent> = arrayListOf()

    var program :Program = Program("","", "","", "","", arrayListOf())
    var placesAndTerritoryObject : Places = Places("", "", "", "", "", arrayListOf(), "")
    var contactAndInfoObject =ContactsAndInfo("","", "", arrayListOf())
    val notificationList : ArrayList<Notifications> = arrayListOf()
    var audiDrivingExperienceObject : AudiDrivingExperience = AudiDrivingExperience("", "", "", "", "", arrayListOf(), "")
    var calendarEventList : ArrayList<CalendarEvent> = arrayListOf()
    val listOfUrls : ArrayList<URL> = arrayListOf()
    val listOfImagesForCalendarEvent :ArrayList<Bitmap> = arrayListOf()
}

// vukovic
data class CalendarEvent(
    val title: String,
    val description: String,
    val image: String
) //napraviti listu eventova
//strana broj 10 skinuti 5 slika


data class FoodExperience(
    val id: String,
    val title: String,
    val header: String,
    val subtitleFood: String,
    val imageBackground: String,
    val programExperience: ArrayList<ProgramExperience> = arrayListOf()
)

data class ProgramExperience(
    val day: String,
    val start: String,
    val type: String,
    val activity: String,
    val site: String,
    val description: String,
    val food: String,
    val allergens: ArrayList<String> = arrayListOf()
)


data class Places(
    val id: String,
    val title: String,
    val subtitle_places: String,
    val title_places: String,
    val description: String,
    val carousel_places: ArrayList<String> = arrayListOf(),
    val image_places: String
)

data class AudiDrivingExperience(
    val id: String,
    val description: String,
    val subtitle_ade: String,
    val main_title: String,
    val title_ade: String,
    val carousel_ade: ArrayList<String> = arrayListOf(),
    val image_ade: String
)

data class  Allergen(
    val name : String
)


// ovde ide i contact  JOVANA TI RADIS OVO
data class ContactsAndInfo(
    val id: String,
    val free_text: String,
    val imageBackground: String,
    val contactList: ArrayList<Contact> = arrayListOf()

)
//jovana 1.1
data class Contact(
    val name: String,
    val place : String,
    val street: String,
    val region: String,
    val phone: String,
    val phoneFix : String,
    val email: String
)
// jovana
// lista notifikacija da se napravi
data class Notifications(
    val date: String,
    val title: String,
    val time: String,
    val description: String

)

//region Programa

data class Activity(
    val start: String,
    val end: String,
    val activityName: String,

    )

data class DailyActivity(
    val day: String,
    val activityList: ArrayList<Activity> = arrayListOf()
)

// jedan objekat  ,u okviru njega activity i daily activity //json
data class Program(
    val image: String,
    val title: String,
    val subTitle:String,
    val note:String,
    val description: String,
    val infoUri:String,
    val dailyActivityList: ArrayList<DailyActivity> = arrayListOf()
)
//endregion

//region Api
data class Header(
    val content_type: String,
)

data class Api(
    val name: String,
    val method: String,
    val url: String,
    val timeout: Int,
    val headers: Header,
)
//endregion

//region Event
interface IEvent {
    val eventId: String
    val titolo: String
    val descrizione: Descrizione
    val data_evento: Date
    val link_myaudi: LinkMyAudi
    val priorita: Int
    val stato: String
    val image: Image
}

data class PublicEvent(
    override val eventId: String,
    override val titolo: String,
    override val descrizione: Descrizione,
    override val data_evento: Date,
    override val link_myaudi: LinkMyAudi,
    override val priorita: Int,
    override val stato: String,
    override val image: Image,

    ) : IEvent

data class PremiumEvent(
    override val eventId: String,      //is the uuid of the public event
    override val titolo: String,       //is the title of the public event
    override val descrizione: Descrizione,  //object that contains two subfields
    val header_premium: String?, // contains the title of the premium event to be displayed in the App PREMIUM
    override val data_evento: Date,         // is the date of the event in the format "2021-02-27"
    override val link_myaudi: LinkMyAudi,   // object that contains an html link
    val noteProgramma: NoteProgramma, //contains the value and format fields with a message about the program for example "the program may vary"
    val programma_dettagliato: ArrayList<ProgrammaDettagliato>, // is an array of objects. Each object represents a day. Each day contains an array of activities.
    val sottotitolo: String, //contains the subtitle of the premium event
    override val priorita: Int,         // represents the importance of the event in increasing order
    override val stato: String,        //the status of the event must always be "active"
    override val image: Image, //contains the information necessary to find the background image associated with the article
) : IEvent

data class Descrizione(
    val format: String, //which is always formatted_text
    val value: String,  //contains the html text entered by the editor
    val processed: String?, //contains the value field surrounded by a <p> </p> tag PREMIUM

)

data class LinkMyAudi(
    val uri: String,   //the url to load
    val title: String, // the text associated with the url
    val options: ArrayList<String> = arrayListOf(), //array of options, typically empty

)

data class NoteProgramma(
    val valueHtmlFormatto: String, //contains program notes
    val format: String, // must be "formatted_text"
    val processed: String,  //contains the value field surrounded by a <p> </p> tag
)

data class ProgrammaDettagliato(
    val programmaDettagliatoList: ArrayList<Activities> = arrayListOf(), //array of objects representing the activities of the day
)

data class Activities(
    val giorno: String, //day of the activity
    val activitiesList: ArrayList<DailyActivity> = arrayListOf(), //Array of activities planned for that day
)

//data class DailyActivity(
//    val dailyActivityList: ArrayList<Activity> = arrayListOf(),
//)
//
//data class Activity(
//    val inizio: String?,  // Start of business - may be empty
//    val fine: String?,    //End of activity - it can be empty
//    val activityHtmlFromattato: String, // Description of the activity - html also long
//)

data class Image(
    val imageInBackgroundId: String, //Alphanumeric Uuid of the Image, it is used in the Api for resizing the images / audi / api / images / resized
    val href: String, // url / path of the image. If it starts with "/ system" it is a private image that must be uploaded through the API / audi / api / images / original
    val meta: Meta,
)

// check is this constants or variables!!!
data class Meta(
    val alt: String,
    val title: String,
    val width: Int,
    val height: Int,
)

//endregion
//region Coupon


interface ICoupon {
    val coupon: String
    val eventId: String
    val status: String
    val valid: String
    val result: String
    val resultCode: String
    val resultMessage: String
}

data class Coupon(
    val action: String?, //the action that was requested
    override val coupon: String, // the coupon code that was passed in the JWT token
    override val eventId: String, // The event code associated with that coupon
    override val status: String, // the status of the coupon, the values "INACTIVE", "ACTIVE", "USED" are possible
    val eventStatus: String?,
    override val valid: String,  //the validity of the coupon: "T" "F"
    override val result: String,        //the result of the operation: "OK", "KO"
    override val resultCode: String, //a string indicating the nature of the error Possible Values
    override val resultMessage: String, // a text message specifying the nature of the problem Default value: empty string
) : ICoupon

enum class Status(s: String) {
    INACTIVE("INACTIVE"),
    ACTIVE("ACTIVE"),
    USED("USED")
}

enum class EventStatus(s: String) {
    Inattivo("I"),
    Attivo("A"),
    Scaduto("S")
}


//endregion


//region URL

val mapOfURL: Map<String, String> =
    mapOf("lang_AudiEventiURL" to "langs/it_IT.json")

//endregion

