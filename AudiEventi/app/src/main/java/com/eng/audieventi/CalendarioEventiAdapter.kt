package com.eng.audieventi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.eventi_cell.view.*


class CalendarioEventiAdapter(val listOfCalendarEvent: ArrayList<CalendarEvent>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.eventi_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.ivEventiImage.setImageBitmap(DataGlobal.listOfImagesForCalendarEvent[position])
        holder.view.tvEventiCalendario.text = listOfCalendarEvent[position].title
        holder.view.tvScopiLeNostreIniziative.text = listOfCalendarEvent[position].description
    }

    override fun getItemCount(): Int {

        return listOfCalendarEvent.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}