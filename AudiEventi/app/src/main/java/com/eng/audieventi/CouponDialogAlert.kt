package com.eng.audieventi

import android.R
import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.coupon_dialog.view.*


class CouponDialogAlert {
    fun showDialog(context: Context) {
        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(com.eng.audieventi.R.layout.coupon_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)

        mDialogView.tvInsertCodeTextDialog.text =
            DataGlobal.dataFunctions.setTextView("COUPON_LOGIN_MESSAGE")
        mDialogView.tvBtnProcedi.text = DataGlobal.dataFunctions.setTextView("COUPON_LOGIN_BUTTON")

        //show dialog
        val mAlertDialog = mBuilder.show()
        //login button click of dialog
        mDialogView.btnProcedi.setOnClickListener {
            val coupon = mDialogView.etInsertCodeDialog.text.toString()

            if(coupon == DataGlobal.coupon.coupon){
                DataGlobal.isVisibleMenuItem = !DataGlobal.isVisibleMenuItem
            }

            //dismiss dialog
            mAlertDialog.dismiss()
            //get text from EditText of dialog


        }
        //cancel button click of dialog
        mDialogView.ivCloseCouponDialog.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
        }
    }

}