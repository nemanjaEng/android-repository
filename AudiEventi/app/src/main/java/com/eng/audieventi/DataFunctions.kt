package com.eng.audieventi

import android.os.AsyncTask
import android.view.View
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

@Suppress("DEPRECATION")
class DataFunctions {

    inner class takeJSON : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {


            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }


            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            DataGlobal.dataFunctions.readLangJson(result)
          //  println(DataGlobal.audiEventiLangs.size.toString())
//            for (keyVal in DataGlobal.audiEventiLangs) {
//                println(keyVal)
//            }
        }
    }

    fun readLangJson(json: String?) {

        val jsonObj = JSONObject(json)
        DataGlobal.numberOfLangs = jsonObj.length()
        DataGlobal.audiEventiLangs = jsonObj.toMap() as Map<String, String>


    }



    fun JSONObject.toMap(): Map<String, Any?> = keys().asSequence().associateWith {

        when (val value = this[it]) {
            is JSONArray -> {
                val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
                JSONObject(map).toMap().values.toList()
            }
            is JSONObject -> value.toMap()
            JSONObject.NULL -> null
            else -> value
        }
    }


    fun setTextView(name: String): String {
        return DataGlobal.audiEventiLangs[name]!!
    }

    fun setTextViewLabelsForHomeFragment(view : View?) {
        view?.tvEventiAudi?.text = DataGlobal.dataFunctions.setTextView("EVENT_LIST_HEADER")
        view?.tvScopiLeNostreIniziative?.text =
            DataGlobal.dataFunctions.setTextView("HOME_GUEST_EVENT_LIST_BUTTON")
        view?.tvIlTuoEvento?.text = DataGlobal.dataFunctions.setTextView("HOME_GUEST_COUPON_LOGIN")
        view?.tvInsericiCoupon?.text =
            DataGlobal.dataFunctions.setTextView("HOME_GUEST_COUPON_LOGIN_BUTTON")
        view?.tvGammaAudi?.text = DataGlobal.dataFunctions.setTextView("HOME_GUEST_AUDI_RANGE")
        view?.tvScopriModelli?.text =
            DataGlobal.dataFunctions.setTextView("HOME_GUEST_AUDI_RANGE_BUTTON")

    }

}