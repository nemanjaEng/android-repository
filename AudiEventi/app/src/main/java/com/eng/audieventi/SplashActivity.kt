package com.eng.audieventi

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.URL
import kotlin.system.measureTimeMillis

val test = TestData()
@Suppress("DEPRECATION")
class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        getSupportActionBar()?.hide()

        val jsonLangURL = DataGlobal.urlAudiEventi + mapOfURL.get("lang_AudiEventiURL")
        DataGlobal.dataFunctions.takeJSON().execute(jsonLangURL)
        TestData().populateTestData()
        parseJsonCalendarEvent(test.calendarEventModelJson)
        getImageFromUrl()
        check()

    }


    fun check() {
        //    println("start")
        Handler(Looper.getMainLooper()).postDelayed({
            println("check numberOfLangs" + DataGlobal.numberOfLangs.toString())
            if (DataGlobal.audiEventiLangs.count() != DataGlobal.numberOfLangs) {
                println("check audiEventiLangs" + DataGlobal.audiEventiLangs.count().toString())
                check()
            } else {
                println(DataGlobal.audiEventiLangs.count().toString())
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }


        }, 100)
    }



    fun parseJsonCalendarEvent(data:String) {


            val calendarJsonArray = JSONArray(data)
        calendarJsonArray.let {
            (0 until it.length()).forEach {
                val jsonObject = calendarJsonArray.getJSONObject(it)
                val title = jsonObject.getString("title")
                val description = jsonObject.getString("description")
                val imageStr = jsonObject.getString("image")

                DataGlobal.calendarEventList?.add(CalendarEvent(
                    title,
                    description,imageStr
                ))

                println("broj objekata" +DataGlobal.calendarEventList.count() )
            }
        }

    }


    // extension function to get / download bitmap from url
    fun URL.toBitmap(): Bitmap? {
        return try {
            BitmapFactory.decodeStream(openStream())
        } catch (e: IOException) {
            null
        }
    }

    fun addUrlToList(){

        for(i in DataGlobal.calendarEventList){
            DataGlobal.listOfUrls.add(URL(i.image))
        }
    }

    fun getImageFromUrl(){
        addUrlToList()
        val deferreds: ArrayList<Deferred<Bitmap?>> = arrayListOf()
        with(GlobalScope) {

            // asynchronously get / download bitmaps from urls

            for (i in DataGlobal.listOfUrls) {
                val time = measureTimeMillis {
                    deferreds.add(async { i.toBitmap() })
                }
                println("curent thread" + Thread.currentThread().name)
                println("time: " + time)
            }

            launch(Dispatchers.Main) {
                // awaits for completion of given deferred
                // values without blocking a thread
                deferreds.awaitAll().apply {

                    for (i in this) {
                        i?.let { DataGlobal.listOfImagesForCalendarEvent?.add(it) }
                    }

                }
            }
        }
    }
}
