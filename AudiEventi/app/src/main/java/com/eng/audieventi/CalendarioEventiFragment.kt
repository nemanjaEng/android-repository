package com.eng.audieventi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_calendario_eventi.*


class CalendarioEventiFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        val view = inflater.inflate(R.layout.fragment_calendario_eventi, container, false)



        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val calendarioEventiAdapter = CalendarioEventiAdapter(DataGlobal.calendarEventList)
        recyclerViewCalendarioEventi.adapter = calendarioEventiAdapter
        recyclerViewCalendarioEventi.setLayoutManager(GridLayoutManager(context, 1))

        super.onViewCreated(view, savedInstanceState)
    }

}