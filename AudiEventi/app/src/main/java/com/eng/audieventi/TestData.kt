package com.eng.audieventi

import android.app.Notification
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL


class TestData {

    fun populateTestData() {
        readContactAndInfoJSON()
        println(DataGlobal.contactAndInfoObject)

        readNotificationJSON()
        println(DataGlobal.notificationList)

        readPlaceAndTerritoryJSON()
        println(DataGlobal.placesAndTerritoryObject)

        readAudiDrivingExperienceJSON()
        println(DataGlobal.audiDrivingExperienceObject)

        readDataCoupon()
        println(DataGlobal.coupon)

        readDataProgramma()
        println(DataGlobal.program)
    }

    val infoAndContactString = """
        {
            "id": "1",
            "free_text": "Per accedere all'attivita di sci s'alpinismo e necessario essere in grado di scendere una pista rossa senza difficolta e avere una preparazione atletica che permetta di affrontare un dislivello positivo di almeno 750mt. in una sessione unica.
                        \n\nL'abbigliamento e l'attrezzatura di sci alpinismo non sono inclusi.\n\nSono obbligatori: sci con attacchi per sci alpinismo, scarponi da sci alpinismo, pelli, casco.\n\nPer shi ha mai fatto questa attivita, il kit antivalanda (pala, sonda e artva) e disponibile su richiesta ed a nollegio.",
            "image_background": "https://audimediacenter-a.akamaihd.net/system/production/media/99949/images/a7d4d13cf6e2d6105f908420c395e1ba077ca6f6/A211724_full.jpg?1617978672",
            "contact_list": [
                {
                    "name": "Segreteria Organizzativa",
                    "place": "",
                    "street": "",
                    "region": "",
                    "phone": "+39 0236705020",
                    "phone_fix": "",
                    "email": "booking@mkeventi.com"
                },
                {
                    "name": "",
                    "place": "Boutique Hodel Diana 4S",
                    "street": "Via Cima Tosa, 52",
                    "region": "38086 Madonna di Campiglio (TN)",
                    "phone": "+39 0465 441011",
                    "phone_fix": "",
                    "email": "info@hoteldiana.net"
                },
                {
                    "name": "",
                    "place": "Alpen Suite Hotel",
                    "street": "viale Dolomiti di Brenta 84",
                    "region": "38086 Madonna di Campiglio (TN)",
                    "phone": "+39 0465 440100",
                    "phone_fix": "+39 0465 440409",
                    "email": "info@alpensuitehotel.it"
                }
            ]
        }
    """.trimIndent()

    val notificationString = """
        {
        "notification_list": [
            {
                "date": "04/04/2021",
                "notifications": [
                    {
                        "title": "Survey disponibile",
                        "time": "18:00",
                        "description": "Abbiamo bisogno del tuo aiuto per capire come migliorare i prossimi Eventi Audi. Partecipa al sondaggio che abbiamo attiato nell'app"
                    },
                    {
                        "title": "Consigli dello staff",
                        "time": "16:00",
                        "description": "Ricordati di portare le Ciaspole"
                    },
                    {
                        "title": "Aggiornamento Programma",
                        "time": "10:00",
                        "description": "Le attiita di Sabato sono state aggiornate. Controlla cosa e cambiato nella sezione dell'app"
                    }
                ]
            },
            {
                "date": "29/03/2021",
                "notifications": [
                    {
                        "title": "Consigli dello staff",
                        "time": "12:00",
                        "description": "Ricordati di passare al punto di accoglienza per registrare la tua partecipazione. Trovi i riferimenti su dove trovarlo nella sezione Contatti dell'app"
                    }
                ]
            }
        ]
        }
    """.trimIndent()

    val place = """
        {
            "id": "1",
            "image_places": "https://cdn.tuttosport.com/img/990/495/2021/01/22/175858111-45480ee5-5fca-4368-a39f-e2b1c6940523.jpg",
            "title": "Audi e Madonna di Campiglio",
            "subtitle_places": "località perfetta per viviere un'esperienza a contatto con la natura",
            "title_places": "Madonna di Campiglio",
            "description": "Performance e sostenibilità nel cuore delle Dolomiti. Goditi l’estate in mezzo ai paesaggi incontaminati di Madonna di Campiglio: a bordo dei modelli elettrici Audi più innovativi, i percorsi tortuosi delle Dolomiti diventano il luogo perfetto per scoprire il nuovo rapporto tra tecnologia e natura della Casa dei quattro anelli. E dove vivere un’esperienza di guida nel pieno rispetto del territorio, ma anche della performance. Per salvaguardare le ricchezze di questa regione, patrimonio dell’Unesco, Audi si impegna ad avere entro il 2025 una gamma di veicoli sempre più sostenibile: 30 modelli a elevata elettrificazione, 20 dei quali totalmente elettrici. Vetture rivoluzionarie come Audi RS e-tron GT e Audi Q4 e-tron che già ci permettono di entrare in contatto con una nuova idea di progresso, in cui efficienza e rispetto per l’ambiente diventano priorità assolute. Scopri qui tutti gli eventi estivi Audi a Madonna di Campiglio!",
            "carousel_places" : [
                "https://www.corriereadriatico.it/photos/MED/99/06/3489906_1511_mdp_0085.jpg",
                "https://giornaledeinavigli.it/media/2019/11/Audi-RS-Q3_001.jpg",
                "https://www.bonaldi.it/data_files/lookbookModel/20210121174159_6009aed781107_A199279_overfull.jpg"
            ]
        }
    """

    val audiDrivingExperience = """
        {
            "id": "1",
            "image_ade": "https://minervamkt.com/wp-content/uploads/caseta_nieve-980x490.png",
            "title_ade": "L'intelligent performance sui nuovo modelli Audi.",
            "subtitle_ade": "Winter Season 2020 2021",
            "description": "Il modello più potente della gamma RS a Madonna di Campiglio: un territorio ricco di luoghi suggestivi e paesaggi naturali che offre un’inedita cornice per scoprire design, innovazione, sostenibilità e progresso. Questa estate, la Casa dei quattro anelli ti accompagnerà in un’esperienza unica per conoscere Audi RS e-tron GT, la Gran Turismo 100% elettrica, incredibilmente potente e innovativa. Lungo le strade panoramiche che dal lago di Garda portano a Madonna di Campiglio, potrai testare a pieno le performance e le qualità dinamiche di questa vera rivoluzione a quattro ruote, nel pieno rispetto dell’ambiente circostante. A tua disposizione, due pacchetti differenti per durata: uno di una giornata intera, l’altro di una giornata e mezza. In entrambe le esperienze testerai Audi RS e-tron GT su un percorso appositamente disegnato, affiancato da professioni esperti che ti forniranno consigli per migliorare la tua tecnica di guida e in totale sicurezza. Inoltre, potrai scoprire a pieno una delle perle delle Dolomiti, ammirandone le bellezze e gustandone i prodotti locali.",
            "main_title": "Audi Driving Experience",
            "carousel_ade" : [
                "https://www.corriereadriatico.it/photos/MED/99/06/3489906_1511_mdp_0085.jpg",
                "https://giornaledeinavigli.it/media/2019/11/Audi-RS-Q3_001.jpg",
                "https://www.bonaldi.it/data_files/lookbookModel/20210121174159_6009aed781107_A199279_overfull.jpg"
            ]
        }
    """

    val calendarEventModelJson: String = """
         [
            {
              "title": "Mountaineering Workshop con Herve",
              "description": "Un evento dedicato alla montagna e al giusto approccio per viverla con rispetto e intelligenza, dalle attivita sportive come lo sci...",
              "image": "https://d2sj0xby2hzqoy.cloudfront.net/audi_events/attachments/data/000/000/412/original/progressofnature.jpg"
            },
            {
              "title": "Audi Talks Performance: ieri, oggi e domani",
              "description": "Otto puntate con ospiti d'eccezione per condividere visioni all'avanguardia e discutere di una nuova, straordinaria idea",
              "image": "https://d2sj0xby2hzqoy.cloudfront.net/audi_events/attachments/data/000/000/413/original/maggy.jpg"
            },
            {
              "title": "Cortina e-portrait",
              "description": "Dal 2017 Audi - per tutelare Cortina come patrimonio si e fatta promotrice con il Comune di Ampezzo di un progetto di Corporate Social",
              "image": "https://d2sj0xby2hzqoy.cloudfront.net/audi_events/attachments/data/000/000/414/original/bellini.jpg"
            },
            {
              "title": "Performance Workshop con Kristian Ghedina",
              "description": "I pluricampioni Kristian Ghedina e Peter Fill, insieme agli chef stellati Costardi Bros, ti guideranno in un'esperienza totale, tra sport",
              "image": "https://d2sj0xby2hzqoy.cloudfront.net/audi_events/attachments/data/000/000/412/original/progressofnature.jpg"
            },
            {
              "title": "Wanderlust con Klaus",
              "description": "il dj e creator Klaus portera a Cortina il suo porgetto Wanderlust, suonando all'alba su una delle piste piu famose: un'esperienza in",
              "image": "https://d2sj0xby2hzqoy.cloudfront.net/audi_events/attachments/data/000/000/415/original/caresexplore.jpg"
            }
         ]
    """.trimIndent()

    val testCoupon: String = """
       {
       "action": "",
       "coupon": "AUDI1",
       "eventId":"EVENT123",
       "status":"ACTIVE",
       "eventStatus":"A",
       "valid": "T",
       "result":"OK",
       "resultCode": "",
       "resultMessage":""
       }
    """

    val foodExperience: String = """
            {
   "id":"",
   "header":"Food Experience",
   "title":"Menu",
   "subtitle":"Scopri le proposte gastronomiche a te riservate",
   "image":"https://figuringoutfoodpod.files.wordpress.com/2019/04/4copper-branch-1600x1172-1080x675.jpg",
   "programExperience":[
      {
         "day":"28-02",
         "start":"07:30",
         "type":"Colazione",
         "activity":"Colazione del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[{
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"28-02",
         "start":"17:30",
         "type":"Pranzo",
         "activity":"Pranzo del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"28-02",
         "start":"17:30",
         "type":"Cena",
         "activity":"Cena del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
         ]
      },
      {
         "day":"29-02",
         "start":"08:30",
         "type":"Colazione",
         "activity":"Colazione del teritorio Rifugio Boch",
         "site":"Hotel",
         "description":"Dolci,pane e marmellate  rigorosamente fatti in casa ,latte e yogurt,provinienti dalla lorem ipsum lorem ipsum lorem ipsum ",
         "food":"Tartellette di confetture bio",
         "allergens":[
           {
         "name":"lattosion"
         },
         {
         "name":"lattosion"
         },
         {
         "name":"frutta a guscio"
         }
            
         ]
      }
   ]
}                                     
        """.trimIndent()


    fun readDataCoupon() {

        val jsonObjCoupon = JSONObject(testCoupon)


        val action = jsonObjCoupon.getString("action")
        val couponProp = jsonObjCoupon.getString("coupon")
        val eventId = jsonObjCoupon.getString("eventId")
        val status = jsonObjCoupon.getString("status")
        val eventStatus = jsonObjCoupon.getString("eventStatus")
        val valid = jsonObjCoupon.getString("valid")
        val result = jsonObjCoupon.getString("result")
        val resultCode = jsonObjCoupon.getString("resultCode")
        val resultMessage = jsonObjCoupon.getString("resultMessage")


        DataGlobal.coupon = Coupon(action,
            couponProp,
            eventId,
            status,
            eventStatus,
            valid,
            result,
            resultCode,
            resultMessage)

    }

    val programaJSONString = """{
	"image": "audi_eventi_program.jpg",
	"title": "Reconnect to nature con Hervé Barmasse",
    "sub_title":"Programma",
	"note": "*il programma potra subire varizaoni.",
	"description": "L’alpinista professionista Hervé Barmasse in una masterclass alla scoperta del profondo legame che lega gli esseri umani alla natura",
	"info_uri": "https://streaming.myaudi.it/eventi/barmasse",
	"daily_activity": [{
	"day": "Venerdi 26 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]},
	{
	"day": "Sabato 28 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]},
	{
	"day": "Domenica 29 Agosto",
	"activity":[{
	"start":"07:30",
	"end": "",
	"activity_name": "Benvenuto e brief tecnico con Hervé Barmasse"},
	{
	"start":"07:30",
	"end": "09:30",
	"activity_name": "Escursionismo"},
	{
	"start":"09:30",
	"end": "11:00",
	"activity_name": "Colazione in rifugio e inspirational talk"},
		{
	"start":"11:30",
	"end": "15:00",
	"activity_name": "Tempo libero"},
		{
	"start":"15:00",
	"end": "16:30",
	"activity_name": "Audi Driving Experience"},
	{
	"start":"Dalle",
	"end": "17:30",
	"activity_name": "Tempo libero e pernottamento"}
	]}]
	
}"""


    fun readDataProgramma() {

        val jsonObjProgram = JSONObject(programaJSONString)
        val listOfDailyActivity: ArrayList<DailyActivity> = arrayListOf()
        val listOfActivity: ArrayList<Activity> = arrayListOf()

        val image = jsonObjProgram.getString("image")
        val title = jsonObjProgram.getString("title")
        val subTitle = jsonObjProgram.getString("sub_title")
        val note = jsonObjProgram.getString("note")
        val description = jsonObjProgram.getString("description")
        val infoUri = jsonObjProgram.getString("info_uri")
        val dailyActivityList = jsonObjProgram.getJSONArray("daily_activity")

        dailyActivityList.let {
            (0 until it.length()).forEach {
                val dailyActivity = dailyActivityList.getJSONObject(it)
                val day = dailyActivity.getString("day")
                val activity = dailyActivity.getJSONArray("activity")

                activity.let {
                    (0 until it.length()).forEach {
                        val singleActivity = activity.getJSONObject(it)
                        val start = singleActivity.getString("start")
                        val end = singleActivity.getString("end")
                        val activityName = singleActivity.getString("activity_name")

                        var act = Activity(start, end, activityName)

                        listOfActivity.add(act)
                    }
                }
                val dailyActivityObj = DailyActivity(day, listOfActivity)

                listOfDailyActivity.add(dailyActivityObj)
            }

        }
        DataGlobal.program =
            Program(image, title, subTitle, note, description, infoUri, listOfDailyActivity)

    }


    fun readContactAndInfoJSON() {
        val contact_list: ArrayList<Contact> = arrayListOf()

        val json = JSONObject(infoAndContactString)

        val id = json.getString("id")
        val free_text = json.getString("free_text")
        val image_background = json.getString("image_background")

        val contact_list_json = json.getJSONArray("contact_list")
        contact_list_json.let {
            (0 until it.length()).forEach {
                val contact = contact_list_json.getJSONObject(it)

                val name = contact.getString("name")
                val place = contact.getString("place")
                val street = contact.getString("street")
                val region = contact.getString("region")
                val phone = contact.getString("phone")
                val phone_fix = contact.getString("phone_fix")
                val email = contact.getString("email")

                contact_list.add(Contact(name, place, street, region, phone, phone_fix, email))

            }
        }


        DataGlobal.contactAndInfoObject = ContactsAndInfo(id, free_text, image_background)
    }

    fun readNotificationJSON() {
        lateinit var notificationsObject: Notifications

        val json = JSONObject(notificationString)

        val notification_list = json.getJSONArray("notification_list")
        notification_list.let {
            (0 until it.length()).forEach {
                val notificationGroup = notification_list.getJSONObject(it)

                val date = notificationGroup.getString("date")

                val notifications = notificationGroup.getJSONArray("notifications")
                notifications.let {
                    (0 until it.length()).forEach {
                        val notification = notifications.getJSONObject(it)

                        val title = notification.getString("title")
                        val time = notification.getString("time")
                        val description = notification.getString("description")

                        notificationsObject = Notifications(date, title, time, description)
                        DataGlobal.notificationList.add(notificationsObject)
                    }
                }
            }
        }
    }

    fun readPlaceAndTerritoryJSON() {
        val carousel_places = arrayListOf<String>()

        val placeJSON = JSONObject(place)

        val id = placeJSON.getString("id")
        val image_places = placeJSON.getString("image_places")
        val title = placeJSON.getString("title")
        val subtitle_places = placeJSON.getString("subtitle_places")
        val title_places = placeJSON.getString("title_places")
        val description = placeJSON.getString("description")


        val carousel = placeJSON.getJSONArray("carousel_places")
        carousel.let {
            (0 until it.length()).forEach {
                val carouselString = carousel.getString(it)
                carousel_places.add(carouselString as String)
            }
        }

        DataGlobal.placesAndTerritoryObject = Places(id,
            title,
            description,
            subtitle_places,
            title_places,
            carousel_places,
            description)
    }

    fun readAudiDrivingExperienceJSON() {
        val audiDrivingExperienceJSON = JSONObject(audiDrivingExperience)

        val id = audiDrivingExperienceJSON.getString("id")

        val image_ade = audiDrivingExperienceJSON.getString("image_ade")
        val main_title = audiDrivingExperienceJSON.getString("main_title")
        val title_ade = audiDrivingExperienceJSON.getString("title_ade")
        val subtitle_ade = audiDrivingExperienceJSON.getString("subtitle_ade")
        val description = audiDrivingExperienceJSON.getString("description")
        val carousel_ade = arrayListOf<String>()
        val carousel = audiDrivingExperienceJSON.getJSONArray("carousel_ade")
        carousel.let {
            (0 until it.length()).forEach {
                val carouselString = carousel.getString(it)
                carousel_ade.add(carouselString as String)
            }
        }

        DataGlobal.audiDrivingExperienceObject = AudiDrivingExperience(id,
            main_title,
            description,
            subtitle_ade,
            title_ade,
            carousel_ade,
            image_ade)
    }




}

// extension function to get / download bitmap from url
fun URL.toBitmap(): Bitmap? {
    return try {
        BitmapFactory.decodeStream(openStream())
    } catch (e: IOException) {
        null
    }
}


//
//  fun readFoodExperienceJson() {
//        var allergenObj : Allergen = Allergen("")
//        val json = JSONObject(foodExperience)
//
//
//        val id = json.getString("id")
//        val header = json.getString("header")
//        val title = json.getString("title")
//        val subtitle = json.getString("subtitle")
//        val image = json.getString("image")
//
//
//        val jsonArrayProgramExperience = json.getJSONArray("programExperience")
//        jsonArrayProgramExperience.let {
//            (0 until it.length()).forEach {
//                val programExperienceObject = jsonArrayProgramExperience.getJSONObject(it)
//                val day = programExperienceObject.getString("day")
//                val start = programExperienceObject.getString("start")
//                val type = programExperienceObject.getString("type")
//                val activity = programExperienceObject.getString("activity")
//                val site = programExperienceObject.getString("site")
//                val description = programExperienceObject.getString("description")
//                val food = programExperienceObject.getString("food")
//                val allergens = programExperienceObject.getJSONArray("allergens")
//                allergens.let {
//                    (0 until it.length()).forEach {
//                        val allergen = allergens.getJSONObject(it)
//                        val name = allergen.getString("name")
//                        allergenObj(name)
//                        DataGlobal.allergensList.add(allergenObj)
//                    }
//                }
//
//
//                val programExperience = DataGlobal.ProgramExperience(
//                    day,
//                    start,
//                    type,
//                    activity,
//                    site,
//                    description,
//                    food,
//                    DataGlobal.allergensList
//                )
//                DataGlobal.programExperienceList.add(programExperience)
//
//                val foodExperience = DataGlobal.FoodExperience(
//                    id,
//                    header,
//                    title,
//                    subtitle,
//                    image,
//                    DataGlobal.programExperienceList
//                )
//
//                DataGlobal.foodExperienceList.add(foodExperience)
//            }
//        }
//
//    }




