package com.eng.pickervezba

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val formatDatuma = SimpleDateFormat("dd MM, YYYY", Locale.US)
    val formatVremena = SimpleDateFormat("HH:mm", Locale.ITALY)
    var jezik: String = ""
    var datum: String = ""
    var vreme: String = ""
    var jezikDatumVreme: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val layoutClick = findViewById<LinearLayout>(R.id.layout)
        layoutClick.setOnClickListener {
            languagePicker()
        }
    }

    fun timePicker() {
        val ispisVreme = findViewById<TextView>(R.id.tvTime)
        val ispis = findViewById<TextView>(R.id.tvIspis)
        val sadasnjeVreme = Calendar.getInstance()
        val pickerVreme =
            TimePickerDialog(

                this, TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                    sadasnjeVreme.set(Calendar.HOUR, hour)
                    sadasnjeVreme.set(Calendar.MINUTE, minute)
                    val ispisVremena = formatVremena.format(sadasnjeVreme.time)
                    ispisVreme.text = ispisVremena
                    jezikDatumVreme += " " + ispisVremena
                    ispis.text = jezikDatumVreme

                },
                sadasnjeVreme.get(Calendar.HOUR),
                sadasnjeVreme.get(Calendar.MINUTE),
                true
            )
        pickerVreme.show()
    }

    fun datePicker() {
        val ispisDatum = findViewById<TextView>(R.id.tvDate)
        val sadasnjeVreme = Calendar.getInstance()

        val pikerDatum = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { picker, year, month, dayOfMonth ->
                val sadasnjeVreme = Calendar.getInstance()
                sadasnjeVreme.set(Calendar.YEAR, year)
                sadasnjeVreme.set(Calendar.MONTH, month)
                sadasnjeVreme.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val ispisDatuma = formatDatuma.format(sadasnjeVreme.time)
                ispisDatum.text = ispisDatuma
                jezikDatumVreme +=  " " + ispisDatuma
                timePicker()

            },
            sadasnjeVreme.get(Calendar.YEAR),
            sadasnjeVreme.get(Calendar.MONTH),
            sadasnjeVreme.get(Calendar.DAY_OF_MONTH)

        )
        pikerDatum.show()

    }

    fun languagePicker() {
        val ispisJezik = findViewById<TextView>(R.id.tvLanguage)
        val nizJezika = arrayOf("Srpski", "Engleski", "Spanski", "Ruski")
        val pickerJezici = AlertDialog.Builder(this)
        pickerJezici.setTitle("Izaberite jezik")
        pickerJezici.setSingleChoiceItems(nizJezika, -1) { dialog, jezik ->
            ispisJezik.text = nizJezika[jezik]
            jezikDatumVreme = nizJezika[jezik]
            dialog.dismiss()
            datePicker()
        }
        val prikazi = pickerJezici.create()
        prikazi.show()
    }

}