package com.eng.personvezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    data class Osoba(
        val ime: String, val prezime: String, val slika: Int
    )

    var osobe: ArrayList<Osoba> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        strechAllColumns()
        createObjectsSource()
        addObjectToTable()


        btnDodaj.setOnClickListener {
            val osoba = Osoba(
                ime = imeEditText.text.toString(),
                prezimeEditText.text.toString(),
               godineEditText.text.toString().toInt()
            );


            osobe.add(osoba)

            tbl.removeAllViews()
            addObjectToTable()
        }
    }

    private fun createObjectsSource() {
        osobe.add(Osoba("Nemanja", "Savic", R.drawable.logo))
        osobe.add(Osoba("Pera", "Peric",  R.drawable.logo))
        osobe.add(Osoba("Petar", "Petrovic",  R.drawable.logo))
    }

    fun strechAllColumns() {
        tbl.apply { isStretchAllColumns = true }
    }

    fun addObjectToTable() {
        for (obj in osobe) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            row.gravity = Gravity.CENTER_HORIZONTAL
            for (count in 1..3) {
            val img = ImageView(this)
                img.setImageResource(obj.slika)
                img.setLayoutParams(TableRow.LayoutParams(50, 50))

                val textView = TextView(this)
                textView.textSize = 20f
                textView.setPadding(20)
                textView.gravity = Gravity.CENTER_HORIZONTAL


                textView.apply {
                    if (count == 1) {
                        textView.text = obj.ime
                    } else if (count == 2) {
                        textView.text = obj.prezime

                    if(count == 3){

                    }
                }

                row.addView(textView)
            }
            tbl.addView(row)
        }
    }

}