package com.eng.swipe_images_vezba

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    var nizSlika: ArrayList<Int> = arrayListOf()
    var nizPozadina: ArrayList<Int> = arrayListOf()
    var brojacPozadine = 0
    var brojacSlike = 0
    var stop = false

    lateinit var gestureDetector: GestureDetector
    lateinit var image: ImageView
    var x1: Float = 0.0F
    var x2: Float = 0.0F
    var y1: Float = 0.0F
    var y2: Float = 0.0F

    val min_Distance = 150

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nizPozadina.add(R.color.black)
        nizPozadina.add(R.color.purple_200)
        nizPozadina.add(R.color.teal_200)

        gestureDetector = GestureDetector(this, this)



    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            1 -> {
                x2 = event.x
                y2 = event.y

                val vrednostX: Float = x2 - x1
                val vrednostY: Float = y2 - y1

                if (abs(vrednostX) > min_Distance) {
                    if (x2 > x1) {
                        --brojacSlike
                        menjajSliku()

                    } else {
                        ++brojacSlike
                        menjajSliku()
                    }
                } else if (abs(vrednostY) > min_Distance) {
                    if (y2 > y1) {
                        //SWIPE DOLE

                        promeniPozadinu()
                        Toast.makeText(this, "možete ubaciti sliku Swipe-om na gore", Toast.LENGTH_SHORT).show()
                    } else {
                        //SWIPE GORE
                        stop = true
                        nizSlika.add(R.drawable.img1)
                        nizSlika.add(R.drawable.img2)
                        nizSlika.add(R.drawable.img3)
                        nizSlika.add(R.drawable.img4)



                        image.setImageResource(nizSlika[0])



                    }
                }
            }
        }
        return gestureDetector.onTouchEvent(event)
    }


    override fun onDown(p0: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(p0: MotionEvent?) {

    }

    override fun onSingleTapUp(p0: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        return false
    }

    override fun onLongPress(p0: MotionEvent?) {
        napraviSliku()
    }

    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        return false
    }

    private fun napraviSliku() {
        val layout = findViewById<ConstraintLayout>(R.id.layout)
         image = ImageView(this)
        layout.addView(image)

        image.layoutParams = ConstraintLayout.LayoutParams(1500, 900)
        image.setBackgroundColor(Color.parseColor("#990000"))
    }

    fun promeniPozadinu(){

        if(!stop){
            Handler(Looper.getMainLooper()).postDelayed({

                image.setImageResource(menjajPozadinu())
                promeniPozadinu()
            }, 500)
        }


    }

    fun menjajPozadinu() :Int{
        if(brojacPozadine == 3){
            brojacPozadine = 0
        }
        val img = nizPozadina[brojacPozadine]
        brojacPozadine +=1
        return  img
    }

    fun menjajSliku() :Int{
        if(brojacSlike == 4){
            brojacSlike = 0
        }
        val img = nizSlika[brojacSlike]
        brojacSlike +=1
        return  img
    }

    fun prikaziSliku(){
        Handler(Looper.getMainLooper()).postDelayed({

            image.setImageResource(menjajSliku())
            prikaziSliku()
        },10000)

    }

}