package com.eng.tablelayoutvezba

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var lista: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        napuniListu()
        dodajUListu()
        for (l in lista) {
            println(l)
        }
        btnAdd.setOnClickListener {
            tbl.removeAllViews()
            lista.add(etUpis.text.toString())
            dodajUListu()
            for (l in lista) {
                println(l)
            }
        }
    }



    fun dodajUListu(){
        for (str in lista) {
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            row.gravity = Gravity.CENTER_HORIZONTAL

            val textView = TextView(this)
            textView.textSize = 30f
            textView.setPadding(20)
            textView.setTextColor(Color.MAGENTA)
            textView.gravity = Gravity.CENTER_HORIZONTAL
            textView.setBackgroundResource(R.color.teal_200)
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);


            textView.apply {
                textView.text = str
            }

            row.addView(textView)
            tbl.addView(row)
        }
    }

    fun napuniListu() {
        lista.add("ASD")
        lista.add("QWE")
        lista.add("DAS")
        lista.add("GFA")
        lista.add("GGG")
        lista.add("HGD")
        lista.add("BXB")
        lista.add("BCX")
        lista.add("CZX")
        lista.add("ZXD")
        lista.add("HRHR")
        lista.add("KIUY")
        lista.add("YKUG")
        lista.add("NGNG")
        lista.add("NFV")
        lista.add("NBVNBV")
        lista.add("ASVXCVD")
        lista.add("VXC")

    }
}