package com.eng.recycleview_i_csv_vezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.BufferedReader
import java.io.InputStreamReader

data class Student(
    val firstName: String,
    val lastName: String,
    val SSN: String,
    val test1: Double?,
    val test2: Double?,
    val test3: Double?,
    val test4: Double?,
    val final: Double?,
    val grade: String
)

class MainActivity : AppCompatActivity() {
    var sviPodaci = ""
    var students: ArrayList<Student> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        parseCSV()

        val recyclerView = findViewById<RecyclerView>(R.id.mojRecyclerView)

        val kreiraniAdapter = MojAdapter(students)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }

    fun parseCSV() {
        var counter = 0
        var line: String?
        val openCSV = InputStreamReader(assets.open("grades.csv"))
        val readLineInCSV = BufferedReader(openCSV)

        // readLineInCSV.readLine()

        while (readLineInCSV.readLine().also {
                line = it
            } != null) {

            val row: List<String> = line!!.split(",")
            // Log.d("CSV", row[0])
            if (counter > 7) {
                if (row[0].isEmpty()) {
                    // Log.d("CSV", row[0])
                } else {

                    sviPodaci =
                        row[0] + " " + row[1] + " " + row[2] + " " + row[3].toDoubleOrNull() + " " + row[4].toDoubleOrNull() + " " + row[5].toDoubleOrNull() + " " + "\n"
                    println(sviPodaci)
                    students.add(
                        Student(

                            row[0],
                            row[1],
                            row[2],
                            row[3].toDoubleOrNull(),
                            row[4].toDoubleOrNull(),
                            row[5].toDoubleOrNull(),
                            row[6].toDoubleOrNull(),
                            row[7].toDoubleOrNull(),
                            row[8]

                        )
                    )
                }
            }
            counter +=1
        }

    }
}