package com.eng.recycleview_i_csv_vezba

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class MojAdapter(val preuzetiPodaci: ArrayList<Student>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {


        val firstName = holder.view.findViewById<TextView>(R.id.tvFirstName)
        firstName.text = preuzetiPodaci[position].firstName.replace("\"", "")

        val lastName = holder.view.findViewById<TextView>(R.id.tvLastName)
        lastName.text = preuzetiPodaci[position].lastName.replace("\"", "")

        val ssn = holder.view.findViewById<TextView>(R.id.tvSSN)
        ssn.text = preuzetiPodaci[position].SSN.replace("\"", "")

        val test1 = holder.view.findViewById<TextView>(R.id.tvTest1)
        test1.text = preuzetiPodaci[position].test1.toString()

        val test2 = holder.view.findViewById<TextView>(R.id.tvTest2)
        if (preuzetiPodaci[position].test2 == null) {
            test2.text = ""
        } else {
            test2.text = preuzetiPodaci[position].test2.toString()
        }
        val test3 = holder.view.findViewById<TextView>(R.id.tvTest3)
        test3.text = preuzetiPodaci[position].test3.toString()

        val test4 = holder.view.findViewById<TextView>(R.id.tvTest4)
        test4.text = preuzetiPodaci[position].test4.toString()

        val final = holder.view.findViewById<TextView>(R.id.tvTestFinal)
        final.text = preuzetiPodaci[position].final.toString()


        val grade = holder.view.findViewById<TextView>(R.id.tvGrade)
        grade.text = preuzetiPodaci[position].grade.replace("\"", "")

    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}
