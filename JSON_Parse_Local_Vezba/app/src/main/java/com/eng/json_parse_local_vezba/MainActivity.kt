package com.eng.json_parse_local_vezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.InputStream
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readJSON()

    }

    private fun readJSON() {

        val json: String?

        try {
            val inputStream: InputStream = assets.open("weather.json")
            json = inputStream.bufferedReader().use {
                it.readText()
            }
            var ispis = ""
            val jsonObj = JSONObject(json)

            val coord = jsonObj.getJSONObject("coord")
            val lon = coord.getDouble("lon")
            val lat = coord.getDouble("lat")
            ispis = lon.toString() + "              " + lat.toString() + "\n"

            val weather = jsonObj.getJSONArray("weather")
            weather.let {
                (0 until it.length()).forEach {
                    val weatherFromArray = weather.getJSONObject(it)
                    val id = weatherFromArray.getInt("id")
                    val mainWeather = weatherFromArray.getString("main")
                    val description = weatherFromArray.getString("description")
                    val icon = weatherFromArray.getString("icon")

                    ispis =
                        ispis + id + "      " + mainWeather + "      " + description + "      " + icon + "\n"

                    tvClouds.text = mainWeather
                    tvDescription.text = description
                }
            }

            val base = jsonObj.getString("base")
            ispis = ispis + base + "\n"

            val main = jsonObj.getJSONObject("main")
            val temp = main.getDouble("temp")
            val feels_like = main.getDouble("feels_like")
            val temp_min = main.getDouble("temp_min")
            val temp_max = main.getDouble("temp_max")
            val pressure = main.getInt("pressure")
            val hummidity = main.getInt("humidity")

            ispis =
                ispis + temp + "      " + feels_like + "      " + temp_min + "      " + temp_max + "      " + pressure + "      " + hummidity + "\n"


            val visibility = jsonObj.getString("visibility")
            ispis = ispis + visibility + "\n"

            val wind = jsonObj.getJSONObject("wind")
            val speed = wind.getDouble("speed")
            val deg = wind.getInt("deg")
            val gust = wind.getDouble("gust")
            ispis = ispis + speed + "            " + deg + "            " + gust + "\n"

            val clouds = jsonObj.getJSONObject("clouds")
            val all = clouds.getInt("all")

            ispis = ispis + all + "\n"
            val dt = jsonObj.getInt("dt")
            ispis = ispis + dt + "\n"

            val sys = jsonObj.getJSONObject("sys")
            val type = sys.getInt("type")
            val idSys = sys.getLong("id")
            val country = sys.getString("country")
            val sunrise = sys.getString("sunrise")
            val sunset = sys.getString("sunset")
            ispis = ispis + type + "      " + idSys + "      "+ country + "\n" + sunrise + "\n" + sunset + "\n"

            val timezone = jsonObj.getInt("timezone")
            val id = jsonObj.getInt("id")
            val name = jsonObj.getString("name")
            val cod = jsonObj.getInt("cod")
            ispis = ispis + timezone + "\n" + id + "\n" + name + "\n" + cod +"\n"




            tvCityName.text = name
            tvCountry.text = country
            tvLon.text = "longitute: " + lon.toString()
            tvLat.text= "lattitude: " + lat.toString()
            tvMain.text = temp.toString()
            tvFeelsLike.text = feels_like.toString()
            tvTempMax.text = temp_max.toString()
            tvTempMin.text = temp_min.toString()
            tvVisibility.text = "Visibility: " + visibility
            tvSpeed.text = speed.toString()
            tvDeg.text = deg.toString()
            tvGust.text = gust.toString()






           // tvIspis.text = ispis
        } catch (ex: Exception) {
            Log.d("ex", ex.toString())
        }
    }
}