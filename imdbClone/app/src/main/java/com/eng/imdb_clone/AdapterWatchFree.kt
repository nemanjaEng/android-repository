package com.eng.imdb_clone

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fan_favorites_cell.view.*
import kotlinx.android.synthetic.main.image_and_title_cell.view.*

class AdapterWatchFree(val list: ArrayList<ImageAndTitle>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.image_and_title_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.imgWideImage.setImageResource(list[position].img)
        holder.view.tvTitleForLargeImage.text = list[position].title

    }

    override fun getItemCount(): Int {
        return list.count()

    }

}