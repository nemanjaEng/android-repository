package com.eng.imdb_clone

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.Serializable
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getSupportActionBar()?.hide()
        val sadrzajFragmenta = findNavController(R.id.fragment)
        bottomNavigationView.setupWithNavController(sadrzajFragmenta)

        for (i in DataGlobal.imagesForSilder) {
            Log.d("IMAGES", i.image.toString())
        }

    }


}