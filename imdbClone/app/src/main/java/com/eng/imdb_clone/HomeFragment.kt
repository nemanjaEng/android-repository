package com.eng.imdb_clone

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_home.*

var imageCounter = 0
var onPause = false

class HomeFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        swapImage()

        DataGlobal.callAdapter(
            requireContext(),
            recyclerViewFeaturedToday,
            AdapterFeaturedToday(DataGlobal.imageForFeaturedToday),
            true)

//        val featuredTodayAdapter = AdapterFeaturedToday(DataGlobal.imageForFeaturedToday)
//        recyclerViewFeaturedToday.adapter = featuredTodayAdapter
//        recyclerViewFeaturedToday.setLayoutManager(LinearLayoutManager(
//            context,
//            LinearLayoutManager.HORIZONTAL,
//            false
//        ))

        DataGlobal.callAdapter(
            requireContext(),
            recyclerViewFanFavorites,
            AdapterAllMovies(DataGlobal.movieList),
            true)

//        val fanFavoritesAdapter = AdapterAllMovies(DataGlobal.movieList)
//        recyclerViewFanFavorites.adapter = fanFavoritesAdapter
//        recyclerViewFanFavorites.setLayoutManager(LinearLayoutManager(
//            context,
//            LinearLayoutManager.HORIZONTAL,
//            false
//        ))
        DataGlobal.callAdapter(
            requireContext(),
            recyclerViewWatchFreeOnIMDb,
            AdapterWatchFree(DataGlobal.imageAndTitleList),
            true)

//        val watchFreeAdapter = AdapterWatchFree(DataGlobal.imageAndTitleList)
//        recyclerViewWatchFreeOnIMDb.adapter = watchFreeAdapter
//        recyclerViewWatchFreeOnIMDb.setLayoutManager(LinearLayoutManager(
//            context,
//            LinearLayoutManager.HORIZONTAL,
//            false
//        ))


    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun onPause() {
        super.onPause()

        onPause = true
        Log.d("pause", onPause.toString())
    }

    override fun onResume() {
        super.onResume()
        onPause = false
        swapImage()
        Log.d("pause", onPause.toString())
    }


    fun swapImage() {
        if (!onPause) {

            DataGlobal.imagesForSilder = arrayListOf()
            DataGlobal.addImagesForSlider()

            if (DataGlobal.imagesForSilder.isEmpty()) {
                Log.d("array", "array is  empty")
            } else {


                if (imageCounter == DataGlobal.imagesForSilder.count()) {
                    imageCounter = 0
                }

                val imgMain = DataGlobal.imagesForSilder[imageCounter]

                if (ivMainImage != null && ivPosterImage != null && tvTitle != null && tvDescription != null) {
                    ivMainImage.setImageResource(imgMain.image)
                    ivPosterImage.setImageResource(imgMain.poster)
                    tvTitle.text = imgMain.title
                    tvDescription.text = imgMain.desc
                }


            }
            Handler(Looper.getMainLooper()).postDelayed({


                imageCounter += 1
                swapImage()

            }, 2500)

        }

    }


}