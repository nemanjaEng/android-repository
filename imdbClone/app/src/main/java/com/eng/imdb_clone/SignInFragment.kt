package com.eng.imdb_clone

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_sign_in.view.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class SignInFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        view.ivBackArrow.setOnClickListener {

            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_youFragment)
        }

        view.tvCreateAnAccount.setOnClickListener {
//            val builder = AlertDialog.Builder(
//                activity, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen)
//
//            builder.setMessage("Alert")
//                .setTitle("Warning")
//
//            val alert: AlertDialog = builder.create()
//            alert.show()

//                val intent = Intent(activity, MainActivity2::class.java)
//                startActivity(intent)


        }


        return view
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SignInFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}