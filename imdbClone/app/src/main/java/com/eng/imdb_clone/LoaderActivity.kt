package com.eng.imdb_clone

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL

class Movie(
    val idMovie: String = "",
    val rank: Int = 0,
    val title: String = "",
    val fullTitle: String = "",
    val year: Int = 0,
    val image: String = "",
    val crew: String = "",
    val imdRating: String = "",
    val imdbRatingCount: String = "",
) : Serializable

class Image(
    val image: Int = 0,
    val poster: Int = 0,
    val title: String = "",
    val desc: String = "",
) : Serializable

class FeaturedToday(
    val image1: Int = 0,
    val image2: Int = 0,
    val image3: Int = 0,
    val imageLarge: Int = 0,
    val text: String = "",

    )

class ImageAndTitle(
    val img: Int = 0,
    val title: String = "",
)

var counter = 0

@Suppress("DEPRECATION")
class LoaderActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loader)
        getSupportActionBar()?.hide()

        val jsonURLtopTenMovies = "https://imdb-api.com/en/API/Top250Movies/k_0jhtjviu"

        val jsonURLmostPopularMovies = "https://imdb-api.com/en/API/MostPopularMovies/k_0jhtjviu"

        getJSONfromURL().execute(jsonURLmostPopularMovies)
        getBitmapFromURL()
        DataGlobal.addImagesForFeaturedToday()
        DataGlobal.addGenresToSmallList()
        DataGlobal.addGenresToLargeList()
        DataGlobal.addImageAndTitle()
        startMainActivityAfterImagesDownloaded()


    }


    fun getBitmapFromURL() {
        if (counter <= DataGlobal.firstTenObjects) {
            Handler(Looper.getMainLooper()).postDelayed({

                getImageFromURL().execute(DataGlobal.movieList[counter].image.replace("original",
                    "192x264"))
                counter += 1
                getBitmapFromURL()
            }, 500)
            println(DataGlobal.imageList.count())
        } else {
            DataGlobal.addImagesForSlider()

        }


    }


    inner class getJSONfromURL : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String {


            var json: String
            val connection = URL(p0[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            getDataFromJSON(result)

        }
    }

    inner class getImageFromURL() : AsyncTask<String, Void, Bitmap?>() {
        override fun doInBackground(vararg p0: String?): Bitmap? {
            var image: Bitmap? = null
            val url = p0[0]

            try {

                val imageFromURL = java.net.URL(url).openStream()
                image = BitmapFactory.decodeStream(imageFromURL)

            } catch (ex: Exception) {
                ex.printStackTrace()
                println(ex.toString())
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            DataGlobal.imageList.add(result!!)

        }

    }

    private fun getDataFromJSON(json: String?) {


        val jsonObj = JSONObject(json)

        val items = jsonObj.getJSONArray("items")

        items.let {
            (0 until it.length()).forEach {
                if (it <= DataGlobal.firstTenObjects) {

                    val itemFromArray = items.getJSONObject(it)
                    val idMovie = itemFromArray.getString("id")
                    val rank = itemFromArray.getInt("rank")
                    val title = itemFromArray.getString("title")
                    val fullTitle = itemFromArray.getString("fullTitle")
                    val year = itemFromArray.getInt("year")
                    val image = itemFromArray.getString("image")
                    val crew = itemFromArray.getString("crew")
                    val imDbRating = itemFromArray.getString("imDbRating")
                    val imDbRatingCount = itemFromArray.getString("imDbRatingCount")
                    val movie = Movie(idMovie,
                        rank,
                        title,
                        fullTitle,
                        year,
                        image,
                        crew,
                        imDbRating,
                        imDbRatingCount)

                    DataGlobal.movieList.add(movie)
                }

            }
        }
    }

    fun startMainActivityAfterImagesDownloaded() {
        Handler(Looper.getMainLooper()).postDelayed({

            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }, 5500)
    }


}