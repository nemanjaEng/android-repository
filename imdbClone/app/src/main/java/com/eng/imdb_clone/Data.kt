package com.eng.imdb_clone

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

object DataGlobal {
    val firstTenObjects = 10

    var movieList: ArrayList<Movie> = arrayListOf()
    var imageList: ArrayList<Bitmap> = arrayListOf()
    var imagesForSilder: ArrayList<Image> = arrayListOf()
    var imageForFeaturedToday: ArrayList<FeaturedToday> = arrayListOf()
    val genresListSmall: ArrayList<String> = arrayListOf()
    val genresListLarge: ArrayList<String> = arrayListOf()
    val imageAndTitleList: ArrayList<ImageAndTitle> = arrayListOf()


    fun addImagesForSlider() {
        imagesForSilder.add(Image(R.drawable.army_of_the_dead,
            R.drawable.army_of_dead_poster,
            "Army of the Dead",
            "Following a zombie outbreak"))
        imagesForSilder.add(Image(R.drawable.avengers,
            R.drawable.avengers_poster,
            "Avengers: End game",
            "After the devastating events of Avengers"))
        imagesForSilder.add(Image(R.drawable.fear_street,
            R.drawable.fear_street_poster,
            "Fear Street",
            "Welcome to Shadyside..."))
        imagesForSilder.add(Image(R.drawable.logo,
            R.drawable.lego_poster,
            "The Lego Movie",
            "An ordinary LEGO construction worker..."))
        imagesForSilder.add(Image(R.drawable.mortal_combat,
            R.drawable.mortal_combat_portrait,
            "Mortal Combat",
            "MMA fighter Cole Young seeks..."))
        imagesForSilder.add(Image(R.drawable.warr,
            R.drawable.warr_poster,
            "War",
            "An Indian soldier chases"))
        imagesForSilder.add(Image(R.drawable.tune_squad,
            R.drawable.tune_squad_poster,
            "Tune Squad",
            "NBA superstar LeBron James..."))

    }

    fun addImagesForFeaturedToday() {
        imageForFeaturedToday.add(FeaturedToday(R.drawable.img1,
            R.drawable.img6,
            R.drawable.img3,
            R.drawable.img1_1,
            "All the Best Shows Coming This Summer"))
        imageForFeaturedToday.add(FeaturedToday(R.drawable.img4,
            R.drawable.img5,
            R.drawable.img2,
            R.drawable.img1_2,
            "Shows Everyone's Still talking About"))
        imageForFeaturedToday.add(FeaturedToday(R.drawable.img7,
            R.drawable.img8,
            R.drawable.img9,
            R.drawable.img1_3,
            "All the Laterst Movies and TV Posters"))
    }

    fun addGenresToSmallList() {
        genresListSmall.add("Movie")
        genresListSmall.add("TV Mini-Series")
        genresListSmall.add("TV Series")
        genresListSmall.add("TV Special")
    }

    fun addGenresToLargeList() {
        genresListLarge.add("Action")
        genresListLarge.add("Adventure")
        genresListLarge.add("Animation")
        genresListLarge.add("Biography")
        genresListLarge.add("Comedy")
        genresListLarge.add("Crime")
        genresListLarge.add("Documentary")
        genresListLarge.add("Drama")
        genresListLarge.add("Family")
        genresListLarge.add("Fantasy")
        genresListLarge.add("History")
        genresListLarge.add("Horror")
        genresListLarge.add("Music")
        genresListLarge.add("Musical")
        genresListLarge.add("Mystery")
        genresListLarge.add("Romance")
        genresListLarge.add("Sci-Fi")
        genresListLarge.add("Thriller")
    }

    fun addImageAndTitle() {
        imageAndTitleList.add(ImageAndTitle(R.drawable.ff1,
            "The 'F9' Family Shares 20 Years of Fast & Furious Memories"))
        imageAndTitleList.add(ImageAndTitle(R.drawable.ff2,
            "Stream the latest full episodes for free online with your TV provider"))
        imageAndTitleList.add(ImageAndTitle(R.drawable.ff3,
            "The Rise of 'F9' Start Sung Kang"))
        imageAndTitleList.add(ImageAndTitle(R.drawable.ff4,
            "The Rise of Charlize Theron"))
        imageAndTitleList.add(ImageAndTitle(R.drawable.ff5,
            "Shows Everyone's Still Talking About"))
    }

    fun callAdapter(
        context: Context,
        recyclerView: RecyclerView,
        adapter: RecyclerView.Adapter<*>?,
        isLinearLayoutManager: Boolean,
    ) {
        //val recyclerView = activity.findViewById<RecyclerView>(recyclerViewId)
        recyclerView.adapter = adapter



        if (isLinearLayoutManager) {
            recyclerView.setLayoutManager(LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL,
                false))
        } else {
            recyclerView.setLayoutManager(GridLayoutManager(context, 1))
        }


    }
}