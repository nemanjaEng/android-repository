package com.eng.imdb_clone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.featured_today_cell.view.*

class AdapterFeaturedToday (val list: ArrayList<FeaturedToday>) : RecyclerView.Adapter<CustomViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.featured_today_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.imgFirst.setImageResource(list[position].image1)
        holder.view.imgSecond.setImageResource(list[position].image2)
        holder.view.imgThird.setImageResource(list[position].image3)
        holder.view.imgLarge.setImageResource(list[position].imageLarge)
        holder.view.tvText.text = list[position].text
    }

    override fun getItemCount(): Int {
       return list.count()

    }
}


    class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    }