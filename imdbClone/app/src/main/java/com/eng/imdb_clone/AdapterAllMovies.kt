package com.eng.imdb_clone

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fan_favorites_cell.view.*
import kotlinx.android.synthetic.main.featured_today_cell.view.*

class AdapterAllMovies (val list: ArrayList<Movie>) : RecyclerView.Adapter<CustomViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.fan_favorites_cell, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.ivMovie.setImageBitmap(DataGlobal.imageList[position])
        holder.view.tvRating.text = list[position].imdRating.toString()
        holder.view.tvMovieName.text =list[position].title
        holder.view.tvDateOfRelease.text = list[position].year.toString()

        if (position%2 == 0 ){
            holder.view.tvButton.text = "+ Watchlist"
        }else
        {
            holder.view.tvButton.text = "Showtimes"
        }

    }

    override fun getItemCount(): Int {
        return list.count()

    }
}