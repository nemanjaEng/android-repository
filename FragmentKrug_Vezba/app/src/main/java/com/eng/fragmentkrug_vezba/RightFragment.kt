package com.eng.fragmentkrug_vezba

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_right.view.*


class RightFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.fragment_right, container, false)

        view.ivRightArrowLeft.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_rightFragment_to_leftFragment)
        }

        view.ivRightHome.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_rightFragment_to_homeFragment)
        }

        return view
    }


}