package com.eng.intent_csv_recyclerview_vezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details_data_global.*

class DetailsActivityDataGlobal : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_data_global)

        tvUsernameDetailsDG.text = DataGlobal.username
        tvIdentifierDetailsDG.text = DataGlobal.identifier.toString()
        tvPasswordDetailsDG.text = DataGlobal.recoveryCode
        tvRecoveryCodeDetailsDG.text = DataGlobal.firstName
        tvFirstNameDetailsDG.text = DataGlobal.lastName
        tvLastNameDetailsDG.text = DataGlobal.department
        tvDepartmentDetailsDG.text = DataGlobal.location
        tvLocationDetailsDG.text = DataGlobal.password

    }
}