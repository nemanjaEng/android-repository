package com.eng.intent_csv_recyclerview_vezba

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.*
import kotlinx.android.synthetic.main.moja_celija.view.*

var rowIndex: Int? = null


class MojAdapter(val preuzetiPodaci: ArrayList<Employee>) :
    RecyclerView.Adapter<CustomViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.tvUsername.text = preuzetiPodaci[position].Username
        holder.view.tvIdentifier.text = preuzetiPodaci[position].identifier.toString()
        holder.view.tvPassword.text = preuzetiPodaci[position].password
        holder.view.tvRecoveryCode.text = preuzetiPodaci[position].recoveryCode
        holder.view.tvFirstName.text = preuzetiPodaci[position].firstName
        holder.view.tvLastName.text = preuzetiPodaci[position].lastName
        holder.view.tvDepartment.text = preuzetiPodaci[position].department
        holder.view.tvLocation.text = preuzetiPodaci[position].location

        holder.view.model.setOnClickListener { v ->
            rowIndex = position
            val intent = Intent(v.context, DetailsActivity::class.java)
            intent.putExtra("username", preuzetiPodaci[position].Username)
            intent.putExtra("identifier", preuzetiPodaci[position].identifier.toString())
            intent.putExtra("password", preuzetiPodaci[position].password)
            intent.putExtra("recoveryCode", preuzetiPodaci[position].recoveryCode)
            intent.putExtra("firstName", preuzetiPodaci[position].firstName)
            intent.putExtra("lastName", preuzetiPodaci[position].lastName)
            intent.putExtra("department", preuzetiPodaci[position].department)
            intent.putExtra("location", preuzetiPodaci[position].location)

            v.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}