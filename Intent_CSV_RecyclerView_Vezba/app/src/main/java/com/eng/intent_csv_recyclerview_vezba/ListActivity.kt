package com.eng.intent_csv_recyclerview_vezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.moja_celija.*
import kotlinx.android.synthetic.main.moja_celija.view.*

class ListActivity : AppCompatActivity() {

    var employeesFromIntent: ArrayList<Employee> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        employeesFromIntent = intent.getSerializableExtra("employeesList") as ArrayList<Employee>

        val kreiraniAdapter = MojAdapter(employeesFromIntent)
        recyclerViewList.adapter = kreiraniAdapter
        recyclerViewList.setLayoutManager(GridLayoutManager(this, 1))


    }

}
