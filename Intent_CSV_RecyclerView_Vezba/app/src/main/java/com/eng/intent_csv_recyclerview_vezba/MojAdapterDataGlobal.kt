package com.eng.intent_csv_recyclerview_vezba

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

var rowIndexDG: Int? = null


class MojAdapterDataGlobal(val preuzetiPodaci: ArrayList<Employee>) :
    RecyclerView.Adapter<CustomViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.tvUsername.text = preuzetiPodaci[position].Username
        holder.view.tvIdentifier.text = preuzetiPodaci[position].identifier.toString()
        holder.view.tvPassword.text = preuzetiPodaci[position].password
        holder.view.tvRecoveryCode.text = preuzetiPodaci[position].recoveryCode
        holder.view.tvFirstName.text = preuzetiPodaci[position].firstName
        holder.view.tvLastName.text = preuzetiPodaci[position].lastName
        holder.view.tvDepartment.text = preuzetiPodaci[position].department
        holder.view.tvLocation.text = preuzetiPodaci[position].location

        holder.view.model.setOnClickListener { v ->
            rowIndex = position
            val intent = Intent(v.context, DetailsActivityDataGlobal::class.java)
            DataGlobal.username =  preuzetiPodaci[position].Username
            DataGlobal.identifier =  preuzetiPodaci[position].identifier
            DataGlobal.password = preuzetiPodaci[position].password
            DataGlobal.recoveryCode =  preuzetiPodaci[position].recoveryCode
            DataGlobal.firstName =  preuzetiPodaci[position].firstName
            DataGlobal.lastName = preuzetiPodaci[position].lastName
            DataGlobal.department =  preuzetiPodaci[position].department
            DataGlobal.location =  preuzetiPodaci[position].location

            v.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }


}

