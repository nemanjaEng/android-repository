package com.eng.intent_csv_recyclerview_vezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable


data class Employee(
    val Username: String,
    val identifier: Int,
    val password: String,
    val recoveryCode: String,
    val firstName: String,
    val lastName: String,
    val department: String,
    val location: String
) : Serializable

class MainActivity : AppCompatActivity() {


    var employees: ArrayList<Employee> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIdiNaListu.setOnClickListener {
            parseCSV()
            val intent = Intent(this, ListActivity::class.java)
            intent.putExtra("employeesList", employees)
            startActivity(intent)
        }

        btnIdiNaListuDataGlobal.setOnClickListener {
            parseCSVforDataGlobal()

            startActivity(Intent(this, DataGlobalListActivity::class.java))

        }


    }

    private fun parseCSV() {
        var line: String?
        val openCSV = InputStreamReader(assets.open("username-password-recovery-code.csv"))
        val readLineInCSV = BufferedReader(openCSV)

        readLineInCSV.readLine()
        while (readLineInCSV.readLine().also {
                line = it
            } != null) {
            val row: List<String> = line!!.split(";")
            if (row[0].isNotEmpty()) {
                employees.add(
                    Employee(

                        row[0],
                        row[1].toInt(),
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6],
                        row[7]
                    )
                )
            }
        }
    }

    private fun parseCSVforDataGlobal() {
        var line: String?
        val openCSV = InputStreamReader(assets.open("username-password-recovery-code.csv"))
        val readLineInCSV = BufferedReader(openCSV)

        readLineInCSV.readLine()
        while (readLineInCSV.readLine().also {
                line = it
            } != null) {
            val row: List<String> = line!!.split(";")
            if (row[0].isNotEmpty()) {
                DataGlobal.employeesDataGlobal.add(
                    Employee(

                        row[0],
                        row[1].toInt(),
                        row[2],
                        row[3],
                        row[4],
                        row[5],
                        row[6],
                        row[7]
                    )
                )
            }
        }
    }
}