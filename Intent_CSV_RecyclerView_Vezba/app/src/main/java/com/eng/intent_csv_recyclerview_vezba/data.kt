package com.eng.intent_csv_recyclerview_vezba

object  DataGlobal {
    var employeesDataGlobal: ArrayList<Employee> = arrayListOf()

    var username: String = ""
    var identifier: Int = 0
    var password: String = ""
    var recoveryCode: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var department: String = ""
    var location: String = ""
}