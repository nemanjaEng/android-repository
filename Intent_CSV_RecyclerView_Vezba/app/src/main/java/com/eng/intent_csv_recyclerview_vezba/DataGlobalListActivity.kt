package com.eng.intent_csv_recyclerview_vezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_data_global_list.*


class DataGlobalListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_global_list)

        val kreiraniAdapter = MojAdapterDataGlobal(DataGlobal.employeesDataGlobal)
        recyclerViewDataGlobal.adapter = kreiraniAdapter
        recyclerViewDataGlobal.setLayoutManager(GridLayoutManager(this, 1))
    }
}