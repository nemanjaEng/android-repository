package com.eng.intent_csv_recyclerview_vezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.moja_celija.*

class DetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val usernameDetails = intent.getStringExtra("username")
        val identifierDetails = intent.getStringExtra("identifier")
        val passwordDetails = intent.getStringExtra("password")
        val recoveryCodeDetails = intent.getStringExtra("recoveryCode")
        val firstNameDetails = intent.getStringExtra("firstName")
        val lastNameDetails = intent.getStringExtra("lastName")
        val departmentDetails = intent.getStringExtra("department")
        val locationDetails = intent.getStringExtra("location")


        tvUsernameDetails.text = usernameDetails.toString()
        tvIdentifierDetails.text = identifierDetails.toString()
        tvPasswordDetails.text = passwordDetails.toString()
        tvRecoveryCodeDetails.text = recoveryCodeDetails.toString()
        tvFirstNameDetails.text = firstNameDetails.toString()
        tvLastNameDetails.text = lastNameDetails.toString()
        tvDepartmentDetails.text = departmentDetails.toString()
        tvLocationDetails.text = locationDetails.toString()
    }
}