package com.eng.prosledjivanjepodatakavezba

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new.*

var podatak = ""


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prosledi.setOnClickListener {
            podatak = parametar.text.toString()
            startActivity(Intent(this, NewActivity::class.java))

            if (podatak == "0"){
                newLayout.setBackgroundColor(Color.parseColor("#990000"))
            }else if (podatak == "1"){
                newLayout.setBackgroundColor(Color.parseColor("#c1c1c1"))
            }
        }
    }
}